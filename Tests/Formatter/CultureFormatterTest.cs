using System.Globalization;
using DKX.NMoney;
using DKX.NMoney.Formatter;
using Xunit;

namespace DKX.NMoneyTests.Formatter;

public sealed class CultureFormatterTest
{
	private readonly CultureFormatter _sut;

	public CultureFormatterTest()
	{
		var cultureInfo = (CultureInfo)new CultureInfo("en-US").Clone();
		cultureInfo.NumberFormat.CurrencyNegativePattern = 1;

		_sut = new CultureFormatter(cultureInfo.NumberFormat);
	}

	[Theory]
	[InlineData(100, "USD", 2, "$1.00", "$")]
	[InlineData(41, "USD", 2, "$0.41", "$")]
	[InlineData(5, "USD", 2, "$0.05", "$")]
	[InlineData(5, "EUR", 2, "€0.05", "€")]
	[InlineData(50, "EUR", 2, "€0.50", "€")]
	[InlineData(500, "EUR", 2, "€5.00", "€")]
	public void Format(long amount, string currency, byte minorUnit, string result, string symbol)
	{
		// Arrange
		var m = new Money(amount, new Currency(currency, currency, minorUnit, 0, symbol));

		// Act
		var actual = _sut.Format(m);

		// Assert
		Assert.Equal(result, actual);
	}

	[Theory]
	[InlineData(5005, "USD", 2, "$50", 0)]
	[InlineData(5, "USD", 2, "$0.050", 3)]
	[InlineData(35, "USD", 2, "$0.350", 3)]
	[InlineData(135, "USD", 2, "$1.350", 3)]
	[InlineData(6135, "USD", 2, "$61.350", 3)]
	[InlineData(-6135, "USD", 2, "-$61.350", 3)]
	[InlineData(-6152, "USD", 2, "-$61.5", 1)]
	[InlineData(5055, "USD", 2, "$51", 0)]
	public void Format_WithMinorUnits(
		long amount,
		string currency,
		byte minorUnit,
		string result,
		byte resultMinorUnits
	)
	{
		// Arrange
		var m = new Money(amount, new Currency(currency, currency, minorUnit, 0, "$"));

		// Act
		var actual = _sut.Format(m, resultMinorUnits);

		// Assert
		Assert.Equal(result, actual);
	}
}
