using DKX.NMoney;
using DKX.NMoney.Formatter;
using Xunit;

namespace DKX.NMoneyTests.Formatter;

public sealed class DecimalFormatterTest
{
	private readonly DecimalFormatter _sut = new();

	[Theory]
	[InlineData(5005, "USD", "50.05")]
	[InlineData(100, "USD", "1.00")]
	[InlineData(41, "USD", "0.41")]
	[InlineData(5, "USD", "0.05")]
	[InlineData(-6152, "USD", "-61.52")]
	[InlineData(5, "JPY", "5")]
	[InlineData(50, "JPY", "50")]
	[InlineData(500, "JPY", "500")]
	[InlineData(-5055, "JPY", "-5055")]
	[InlineData(50050050, "USD", "500500.50")]
	public void Format(long amount, string currency, string result)
	{
		// Arrange
		var m = new Money(amount, new Currency(currency));

		// Act
		var actual = _sut.Format(m);

		// Assert
		Assert.Equal(result, actual);
	}

	[Theory]
	[InlineData(50, "USD", 3, "0.050")]
	[InlineData(350, "USD", 3, "0.350")]
	[InlineData(1357, "USD", 3, "1.357")]
	[InlineData(61351, "USD", 3, "61.351")]
	[InlineData(-61351, "USD", 3, "-61.351")]
	[InlineData(5, "JPY", 2, "0.05")]
	[InlineData(50, "JPY", 2, "0.50")]
	[InlineData(500, "JPY", 2, "5.00")]
	[InlineData(-5055, "JPY", 2, "-50.55")]
	public void Format_WithMinorUnits(
		long amount,
		string currency,
		byte resultMinorUnit,
		string result
	)
	{
		// Arrange
		var m = new Money(amount, new Currency(currency));

		// Act
		var actual = _sut.Format(m, resultMinorUnit);

		// Assert
		Assert.Equal(result, actual);
	}
}
