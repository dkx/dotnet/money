using System.Text.Json;
using System.Text.Json.Serialization;
using DKX.NMoney;
using DKX.NMoney.CurrencyList;
using DKX.NMoney.Json;
using Xunit;

namespace DKX.NMoneyTests.Json;

public sealed class MoneyConverterTest
{
	private readonly JsonSerializerOptions _options;

	public MoneyConverterTest()
	{
		_options = new JsonSerializerOptions();
		_options.Converters.Add(new MoneyConverter(IsoCurrencyList.Instance));
		_options.WriteIndented = true;
	}

	[Fact]
	public void Serialize_ShouldTransformToJson()
	{
		// Arrange
		var data = new Data { Price = new Money(500L, new Currency("EUR")) };

		// Act
		var json = JsonSerializer.Serialize(data, _options).Replace(Environment.NewLine, "\n");

		// Assert
		Assert.Equal(
			@"{
  ""price"": {
    ""currency"": ""EUR"",
    ""amount"": ""500""
  }
}",
			json
		);
	}

	[Fact]
	public void Deserialize_ShouldThrow_WhenGivenInvalidObject()
	{
		// Arrange
		const string json = @"{""price"": 600}";

		// Act
		object? Fn() => JsonSerializer.Deserialize<Data>(json, _options);

		// Assert
		Assert.Throws<JsonException>(Fn);
	}

	[Fact]
	public void Deserialize_ShouldThrow_WhenMissingCurrency()
	{
		// Arrange
		const string json = @"{""price"": {}}";

		// Act
		object? Fn() => JsonSerializer.Deserialize<Data>(json, _options);

		// Assert
		Assert.Throws<JsonException>(Fn);
	}

	[Fact]
	public void Deserialize_ShouldThrow_WhenGivenInvalidCurrency()
	{
		// Arrange
		var json =
			@"{
  ""price"": {
    ""currency"": 500
  }
}";

		// Act
		object? Fn() => JsonSerializer.Deserialize<Data>(json, _options);

		// Assert
		Assert.Throws<JsonException>(Fn);
	}

	[Fact]
	public void Deserialize_ShouldThrow_WhenMissingAmount()
	{
		// Arrange
		var json =
			@"{
  ""price"": {
    ""currency"": ""EUR""
  }
}";

		// Act
		object? Fn() => JsonSerializer.Deserialize<Data>(json, _options);

		// Assert
		Assert.Throws<JsonException>(Fn);
	}

	[Fact]
	public void Deserialize_ShouldThrow_WhenGivenInvalidAmount()
	{
		// Arrange
		var json =
			@"{
  ""price"": {
    ""currency"": ""EUR"",
    ""amount"": 500
  }
}";

		// Act
		object? Fn() => JsonSerializer.Deserialize<Data>(json, _options);

		// Assert
		Assert.Throws<JsonException>(Fn);
	}

	[Fact]
	public void Deserialize_ShouldThrow_WhenGivenUnknownProperty()
	{
		// Arrange
		var json =
			@"{
  ""price"": {
    ""currency"": ""EUR"",
    ""amount"": ""500"",
    ""unknown"": ""property""
  }
}";

		// Act
		object? Fn() => JsonSerializer.Deserialize<Data>(json, _options);

		// Assert
		Assert.Throws<JsonException>(Fn);
	}

	[Fact]
	public void Deserialize()
	{
		// Arrange
		var json =
			@"{
  ""price"": {
    ""currency"": ""EUR"",
    ""amount"": ""500""
  }
}";
		var expected = new Money(500, new Currency("EUR"));

		// Act
		var data = JsonSerializer.Deserialize<Data>(json, _options)!;

		// Assert
		Assert.Equal(expected, data.Price);
	}

	private sealed class Data
	{
		[JsonPropertyName("price")]
		public Money Price { get; init; }
	}
}
