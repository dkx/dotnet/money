using DKX.NMoney;
using DKX.NMoney.Parser;
using Xunit;

namespace DKX.NMoneyTests.Parser;

public sealed class DecimalParserTest
{
	private readonly DecimalParser _sut = new();

	[Fact]
	public void Parse_ShouldThrow_WhenCurrencyWasNotProvided()
	{
		Assert.Throws<NotImplementedException>(() => _sut.Parse("1000"));
	}

	[Theory]
	[InlineData("INVALID")]
	[InlineData(".")]
	public void Parse_ShouldThrow_WhenGivenInvalidMoney(string money)
	{
		// Arrange
		var currency = new Currency("USD");

		// Act
		object Fn() => _sut.Parse(money, currency);

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Theory]
	[InlineData("1000.50", "USD", 100050)]
	[InlineData("1000.00", "USD", 100000)]
	[InlineData("1000.0", "USD", 100000)]
	[InlineData("1000", "USD", 100000)]
	[InlineData("0.01", "USD", 1)]
	[InlineData("0.00", "USD", 0)]
	[InlineData("1", "USD", 100)]
	[InlineData("-1000.50", "USD", -100050)]
	[InlineData("-1000.00", "USD", -100000)]
	[InlineData("-1000.0", "USD", -100000)]
	[InlineData("-1000", "USD", -100000)]
	[InlineData("-0.01", "USD", -1)]
	[InlineData("-1", "USD", -100)]
	[InlineData("1000.50", "JPY", 1001)]
	[InlineData("1000.00", "JPY", 1000)]
	[InlineData("1000.0", "JPY", 1000)]
	[InlineData("1000", "JPY", 1000)]
	[InlineData("0.01", "JPY", 0)]
	[InlineData("1", "JPY", 1)]
	[InlineData("-1000.50", "JPY", -1001)]
	[InlineData("-1000.00", "JPY", -1000)]
	[InlineData("-1000.0", "JPY", -1000)]
	[InlineData("-1000", "JPY", -1000)]
	[InlineData("-0.01", "JPY", -0)]
	[InlineData("-1", "JPY", -1)]
	[InlineData("", "USD", 0)]
	[InlineData(".99", "USD", 99)]
	[InlineData("99.", "USD", 9900)]
	[InlineData("-9.999", "USD", -1000)]
	[InlineData("9.999", "USD", 1000)]
	[InlineData("9.99", "USD", 999)]
	[InlineData("-9.99", "USD", -999)]
	public void Parse(string money, string currency, long result)
	{
		// Arrange
		var c = new Currency(currency);

		// Act
		var actual = _sut.Parse(money, c, MidpointRounding.AwayFromZero);

		// Assert
		Assert.Equal(result, actual.Amount);
		Assert.Equal(currency, actual.Currency.Code);
	}

	[Theory]
	[InlineData("1000.501", "USD", 3, 1000501)]
	[InlineData("1000.001", "USD", 3, 1000001)]
	[InlineData("1000.50", "USD", 3, 1000500)]
	[InlineData("1000.00", "USD", 3, 1000000)]
	[InlineData("1000.0", "USD", 3, 1000000)]
	[InlineData("1000", "USD", 3, 1000000)]
	[InlineData("0.001", "USD", 3, 1)]
	[InlineData("0.01", "USD", 3, 10)]
	[InlineData("1", "USD", 3, 1000)]
	[InlineData("-1000.501", "USD", 3, -1000501)]
	[InlineData("-1000.001", "USD", 3, -1000001)]
	[InlineData("-1000.50", "USD", 3, -1000500)]
	[InlineData("-1000.00", "USD", 3, -1000000)]
	[InlineData("-1000.0", "USD", 3, -1000000)]
	[InlineData("-1000", "USD", 3, -1000000)]
	[InlineData("-0.001", "USD", 3, -1)]
	[InlineData("-0.01", "USD", 3, -10)]
	[InlineData("-1", "USD", 3, -1000)]
	public void Parse_WithMinorUnits(
		string money,
		string currency,
		byte resultMinorUnits,
		long result
	)
	{
		// Arrange
		var c = new Currency(currency);

		// Act
		var actual = _sut.Parse(money, c, resultMinorUnits, MidpointRounding.AwayFromZero);

		// Assert
		Assert.Equal(result, actual.Amount);
		Assert.Equal(currency, actual.Currency.Code);
	}
}
