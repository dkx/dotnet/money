using DKX.NMoney;
using DKX.NMoney.CurrencyList;
using Xunit;

namespace DKX.NMoneyTests;

public sealed class CurrencyTest
{
	[Fact]
	public void Constructor_ShouldCreateCurrency_WhenGivenWithFullConfiguration()
	{
		// Act
		var c = new Currency("EUR", "Euro", 2, 978, "€");

		// Assert
		Assert.Equal("EUR", c.Code);
		Assert.Equal("Euro", c.Name);
		Assert.Equal(2, c.MinorUnit);
		Assert.Equal(978, c.NumericCode);
		Assert.Equal("€", c.Symbol);
	}

	[Fact]
	public void Constructor_ShouldCreateIsoCurrency_WhenGivenOnlyCode()
	{
		// Act
		var c = new Currency("EUR");

		// Assert
		Assert.Equal("EUR", c.Code);
		Assert.Equal("Euro", c.Name);
		Assert.Equal(2, c.MinorUnit);
		Assert.Equal(978, c.NumericCode);
		Assert.Equal("€", c.Symbol);
	}

	[Fact]
	public void Constructor_ShouldCreateCryptoCurrency_WhenGivenCryptoCurrencyList()
	{
		// Arrange
		var list = CryptoCurrencyList.Instance;

		// Act
		var c = new Currency("XBT", list);

		// Assert
		Assert.Equal("XBT", c.Code);
		Assert.Equal("Bitcoin", c.Name);
		Assert.Equal(8, c.MinorUnit);
		Assert.Equal(0, c.NumericCode);
		Assert.Equal('\u20BF'.ToString(), c.Symbol);
	}

	[Fact]
	public void Equals_ShouldBeTrue_WhenComparingToItself()
	{
		// Arrange
		var c = new Currency("EUR");

		// Act
		var result = c.Equals(c);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void Equals_ShouldBeTrue_WhenHavingSameCurrencyCode()
	{
		// Arrange
		var a = new Currency("EUR");
		var b = new Currency("EUR");

		// Act
		var result = a.Equals(b);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void Equals_ShouldBeFalse_WhenNotComparingToCurrencyStruct()
	{
		// Arrange
		var c = new Currency("EUR");

		// Act
		// ReSharper disable once SuspiciousTypeConversion.Global
		var result = c.Equals("EUR");

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void Equals_ShouldBeFalse_WhenHavingDifferentCurrencyCodes()
	{
		// Arrange
		var a = new Currency("EUR");
		var b = new Currency("USD");

		// Act
		var result = a.Equals(b);

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void OperatorEquals_ShouldBeTrue_WhenHavingSameCurrencyCode()
	{
		// Arrange
		var a = new Currency("EUR");
		var b = new Currency("EUR");

		// Act
		var result = a == b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorEquals_ShouldBeFalse_WhenHavingDifferentCurrencyCodes()
	{
		// Arrange
		var a = new Currency("EUR");
		var b = new Currency("USD");

		// Act
		var result = a == b;

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void OperatorNotEquals_ShouldBeTrue_WhenHavingDifferentCurrencyCodes()
	{
		// Arrange
		var a = new Currency("EUR");
		var b = new Currency("USD");

		// Act
		var result = a != b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorNotEquals_ShouldBeFalse_WhenHavingSameCurrencyCode()
	{
		// Arrange
		var a = new Currency("EUR");
		var b = new Currency("EUR");

		// Act
		var result = a != b;

		// Assert
		Assert.False(result);
	}
}
