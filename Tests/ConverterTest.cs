using DKX.NMoney;
using NSubstitute;
using Xunit;

namespace DKX.NMoneyTests;

public sealed class ConverterTest
{
	private readonly IExchange _exchange = Substitute.For<IExchange>();

	private readonly Converter _sut;

	public ConverterTest()
	{
		_sut = new Converter(_exchange);
	}

	[Theory]
	[MemberData(nameof(ConvertData))]
	public async Task Convert(
		string fromCurrencyCode,
		string toCurrencyCode,
		byte fromCurrencyMinorUnits,
		byte toCurrencyMinorUnits,
		decimal ratio,
		long amount,
		long result
	)
	{
		// Arrange
		var fromCurrency = new Currency(
			fromCurrencyCode,
			fromCurrencyCode,
			fromCurrencyMinorUnits,
			0,
			Currency.GenericCurrencySign
		);
		var toCurrency = new Currency(
			toCurrencyCode,
			toCurrencyCode,
			toCurrencyMinorUnits,
			1,
			Currency.GenericCurrencySign
		);
		_exchange
			.GetConversionRatio(fromCurrency, toCurrency)
			.Returns(Task.FromResult(new ExchangeRate(fromCurrency, toCurrency, ratio)));

		// Act
		var converted = await _sut.Convert(
			new Money(amount, fromCurrency),
			toCurrency,
			MidpointRounding.AwayFromZero
		);

		// Assert
		Assert.Equal(result, converted.Amount);
		Assert.Equal(toCurrency, converted.Currency);
	}

	public static IEnumerable<object[]> ConvertData()
	{
		yield return new object[] { "USD", "JPY", 2, 0, 101M, 100L, 101L };
		yield return new object[] { "JPY", "USD", 0, 2, 0.0099M, 1000L, 990L };
		yield return new object[] { "USD", "EUR", 2, 2, 0.89M, 100L, 89L };
		yield return new object[] { "EUR", "USD", 2, 2, 1.12M, 100L, 112L };
		yield return new object[] { "XBT", "USD", 8, 2, 6597M, 1L, 0L };
		yield return new object[] { "XBT", "USD", 8, 2, 6597M, 10L, 0L };
		yield return new object[] { "XBT", "USD", 8, 2, 6597M, 100L, 1L };
		yield return new object[] { "ETH", "EUR", 18, 2, 330.84M, 100000000000000000L, 3308L };
		yield return new object[] { "BTC", "USD", 8, 2, 13500M, 100000000L, 1350000L };
		yield return new object[]
		{
			"USD",
			"BTC",
			2,
			8,
			decimal.Divide(1, 13500),
			1350000L,
			100000000L,
		};
	}
}
