using DKX.NMoney;
using Xunit;

namespace DKX.NMoneyTests;

public sealed class MoneyTest
{
	[Fact]
	public void Constructor_ShouldCreateMoney_WhenGivenCurrency()
	{
		// Arrange
		var c = new Currency("EUR");

		// Act
		var m = new Money(0, c);

		// Assert
		Assert.Equal(0, m.Amount);
		Assert.Equal(c, m.Currency);
	}

	[Fact]
	public void Constructor_ShouldCreateMoney_WhenGivenCurrencyCode()
	{
		// Arrange
		var c = new Currency("EUR");

		// Act
		var m = new Money(0, "EUR");

		// Assert
		Assert.Equal(0, m.Amount);
		Assert.Equal(c, m.Currency);
	}

	[Theory]
	[InlineData(10, "USD", 1000)]
	[InlineData(10.01, "USD", 1001)]
	[InlineData(10.805, "JPY", 11)]
	public void Constructor_ShouldCreateMoney_WhenGivenDecimalAmount(
		decimal amount,
		string currencyCode,
		long result
	)
	{
		// Arrange
		var currency = new Currency(currencyCode);

		// Act
		var m = new Money(amount, currency);

		// Assert
		Assert.Equal(result, m.Amount);
		Assert.Equal(currency, m.Currency);
	}

	[Theory]
	[InlineData(10, "USD", 1000)]
	[InlineData(10.01, "USD", 1001)]
	[InlineData(10.805, "JPY", 11)]
	public void Constructor_ShouldCreateMoney_WhenGivenDecimalAmountWithCurrencyCode(
		decimal amount,
		string currencyCode,
		long result
	)
	{
		// Arrange
		var currency = new Currency(currencyCode);

		// Act
		var m = new Money(amount, currencyCode);

		// Assert
		Assert.Equal(result, m.Amount);
		Assert.Equal(currency, m.Currency);
	}

	[Fact]
	public void NewInstance_ShouldCreateNewMoney_WhenGivenLongAmount()
	{
		// Arrange
		var m = new Money(0, new Currency("EUR"));

		// Act
		var clone = m.NewInstance(10);

		// Assert
		Assert.NotEqual(m, clone);
		Assert.Equal(m.Currency, clone.Currency);
		Assert.Equal(0, m.Amount);
		Assert.Equal(10, clone.Amount);
	}

	[Fact]
	public void NewInstance_ShouldCreateNewMoney_WhenGivenDecimalAmount()
	{
		// Arrange
		var m = new Money(0, new Currency("EUR"));

		// Act
		var clone = m.NewInstance(10M);

		// Assert
		Assert.NotEqual(m, clone);
		Assert.Equal(m.Currency, clone.Currency);
		Assert.Equal(0, m.Amount);
		Assert.Equal(1000, clone.Amount);
	}

	[Fact]
	public void IsSameCurrency_ShouldBeTrue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(0, c);

		// Act
		var result = a.IsSameCurrency(b);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void IsSameCurrency_ShouldBeFalse()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		var result = a.IsSameCurrency(b);

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void AssertSameCurrency_ShouldThrow_WhenDifferentCurrencies()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		void Fn() => a.AssertSameCurrency(b);

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void GetHashCode_ShouldBeEqual_WhenHavingSameAmountAndCurrency()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(500, c);
		var b = new Money(500, c);

		// Act
		var hashCodeA = a.GetHashCode();
		var hashCodeB = b.GetHashCode();

		// Assert
		Assert.Equal(hashCodeA, hashCodeB);
	}

	[Fact]
	public void Equals_ShouldBeTrue_WhenTestingOnSelf()
	{
		// Arrange
		var m = new Money(0, new Currency("EUR"));

		// Act
		var result = m.Equals(m);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void Equals_ShouldBeTrue_WhenHavingSameAmountAndCurrency()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(0, c);

		// Act
		var result = a.Equals(b);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void Equals_ShouldBeFalse_WhenNotTestingOnMoney()
	{
		// Arrange
		var m = new Money(0, new Currency("EUR"));

		// Act
		// ReSharper disable once SuspiciousTypeConversion.Global
		var result = m.Equals(0);

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void Equals_ShouldBeFalse_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		var result = a.Equals(b);

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void Equals_ShouldBeFalse_WhenHavingDifferentAmount()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(10, c);

		// Act
		var result = a.Equals(b);

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void CompareTo_ShouldThrow_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		object Func() => a.CompareTo(b);

		// Assert
		Assert.Throws<ArgumentException>(Func);
	}

	[Fact]
	public void CompareTo_ShouldBeLessThan()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(5, c);

		// Act
		var result = a.CompareTo(b);

		// Assert
		Assert.Equal(-1, result);
	}

	[Fact]
	public void CompareTo_ShouldBeEqual()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(5, c);

		// Act
		var result = a.CompareTo(b);

		// Assert
		Assert.Equal(0, result);
	}

	[Fact]
	public void CompareTo_ShouldBeGreaterThan()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(0, c);

		// Act
		var result = a.CompareTo(b);

		// Assert
		Assert.Equal(1, result);
	}

	[Fact]
	public void GreaterThan_ShouldThrow_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		object Fn() => a.GreaterThan(b);

		// Assert
		Assert.Throws<ArgumentException>((Func<object>)Fn);
	}

	[Fact]
	public void GreaterThan_ShouldBeTrue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(10, c);
		var b = new Money(0, c);

		// Act
		var result = a.GreaterThan(b);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void GreaterThan_ShouldBeFalse()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(10, c);

		// Act
		var result = a.GreaterThan(b);

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void GreaterThanOrEqual_ShouldThrow_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		object Fn() => a.GreaterThanOrEqual(b);

		// Assert
		Assert.Throws<ArgumentException>((Func<object>)Fn);
	}

	[Fact]
	public void GreaterThanOrEqual_ShouldBeTrue_WhenOtherIsGreater()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(10, c);
		var b = new Money(0, c);

		// Act
		var result = a.GreaterThanOrEqual(b);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void GreaterThanOrEqual_ShouldBeTrue_WhenEqual()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(10, c);
		var b = new Money(10, c);

		// Act
		var result = a.GreaterThanOrEqual(b);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void GreaterThanOrEqual_ShouldBeFalse()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(10, c);

		// Act
		var result = a.GreaterThanOrEqual(b);

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void LessThan_ShouldThrow_WhenHavingDifferentCurrency()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		object Fn() => a.LessThan(b);

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void LessThan_ShouldBeTrue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(10, c);

		// Act
		var result = a.LessThan(b);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void LessThan_ShouldBeFalse()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(10, c);
		var b = new Money(0, c);

		// Act
		var result = a.LessThan(b);

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void LessThanOrEqual_ShouldThrow_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		object Fn() => a.LessThanOrEqual(b);

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void LessThanOrEqual_ShouldBeTrue_WhenLessThan()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(10, c);

		// Act
		var result = a.LessThanOrEqual(b);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void LessThanOrEqual_ShouldBeTrue_WhenEquals()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(10, c);
		var b = new Money(10, c);

		// Act
		var result = a.LessThanOrEqual(b);

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void LessThanOrEqual_ShouldBeFalse()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(10, c);
		var b = new Money(0, c);

		// Act
		var result = a.LessThanOrEqual(b);

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void Add_ShouldThrow_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		object Fn() => a.Add(b);

		// Assert
		Assert.Throws<ArgumentException>((Func<object>)Fn);
	}

	[Fact]
	public void Add_ShouldAdd_WhenGivenMoneyInstance()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(10, c);

		// Act
		var m = a.Add(b);

		// Assert
		Assert.NotEqual(a, m);
		Assert.Equal(15, m.Amount);
	}

	[Fact]
	public void Add_ShouldAdd_WhenGivenDecimal()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5_00, c);
		var b = 10M;

		// Act
		var m = a.Add(b);

		// Assert
		Assert.NotEqual(a, m);
		Assert.Equal(15_00, m.Amount);
	}

	[Fact]
	public void Subtract_ShouldThrow_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var a = new Money(0, new Currency("EUR"));
		var b = new Money(0, new Currency("USD"));

		// Act
		object Fn() => a.Subtract(b);

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void Subtract_ShouldSubtract_WhenGivenMoneyInstance()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(15, c);
		var b = new Money(5, c);

		// Act
		var m = a.Subtract(b);

		// Assert
		Assert.NotEqual(a, m);
		Assert.Equal(10, m.Amount);
	}

	[Fact]
	public void Subtract_ShouldSubtract_WhenGivenDecimal()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(15_00, c);
		var b = 5M;

		// Act
		var m = a.Subtract(b);

		// Assert
		Assert.NotEqual(a, m);
		Assert.Equal(10_00, m.Amount);
	}

	[Fact]
	public void Multiply_WhenGivenMoney()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5_00, c);
		var b = new Money(2_00, c);

		// Act
		var r = a.Multiply(b);

		// Assert
		Assert.NotEqual(a, r);
		Assert.Equal(10_00, r.Amount);
	}

	[Fact]
	public void Multiply_WhenGivenDecimal()
	{
		// Arrange
		var m = new Money(5, new Currency("EUR"));

		// Act
		var r = m.Multiply(2.5M);

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(12, r.Amount);
	}

	[Fact]
	public void Multiply_WhenGivenDouble()
	{
		// Arrange
		var m = new Money(5, new Currency("EUR"));

		// Act
		var r = m.Multiply(2.5);

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(12, r.Amount);
	}

	[Fact]
	public void Multiply_WhenGivenInt()
	{
		// Arrange
		var m = new Money(5, new Currency("EUR"));

		// Act
		var r = m.Multiply(2);

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(10, r.Amount);
	}

	[Fact]
	public void Divide_ShouldThrow_WhenDividingByZero()
	{
		// Arrange
		var m = new Money(5, new Currency("EUR"));

		// Act
		object Fn() => m.Divide(0M);

		// Assert
		Assert.Throws<DivideByZeroException>((Func<object>)Fn);
	}

	[Fact]
	public void Divide_WhenGivenMoney()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(10_00, c);
		var b = new Money(2_00, c);

		// Act
		var r = a.Divide(b);

		// Assert
		Assert.NotEqual(a, r);
		Assert.Equal(5_00, r.Amount);
	}

	[Fact]
	public void Divide_WhenGivenDecimal()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var r = m.Divide(2M);

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(5, r.Amount);
	}

	[Fact]
	public void Divide_WhenGivenDouble()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var r = m.Divide(2D);

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(5, r.Amount);
	}

	[Fact]
	public void Divide_WhenGivenInt()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var r = m.Divide(2);

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(5, r.Amount);
	}

	[Fact]
	public void Mod_WhenGivenDecimal()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var r = m.Mod(3M);

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(1, r.Amount);
	}

	[Fact]
	public void Mod_WhenGivenDouble()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var r = m.Mod(3D);

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(1, r.Amount);
	}

	[Fact]
	public void Mod_WhenGivenInt()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var r = m.Mod(3);

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(1, r.Amount);
	}

	[Fact]
	public void Abs_Positive()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var r = m.Abs();

		// Assert
		Assert.Equal(m, r);
		Assert.Equal(10, r.Amount);
	}

	[Fact]
	public void Abs_WhenGivenNegative()
	{
		// Arrange
		var m = new Money(-10, new Currency("EUR"));

		// Act
		var r = m.Abs();

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(10, r.Amount);
	}

	[Fact]
	public void Negative_WhenGivenPositive()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var r = m.Negative();

		// Assert
		Assert.NotEqual(m, r);
		Assert.Equal(-10, r.Amount);
	}

	[Fact]
	public void Negative_WhenGivenNegative()
	{
		// Arrange
		var m = new Money(-10, new Currency("EUR"));

		// Act
		var r = m.Negative();

		// Assert
		Assert.Equal(m, r);
		Assert.Equal(-10, r.Amount);
	}

	[Fact]
	public void Allocate_ShouldThrow_WhenGivenEmptyCollection()
	{
		// Arrange
		var m = new Money(100, new Currency("EUR"));

		// Act
		object Fn() => m.Allocate(Array.Empty<decimal>());

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void Allocate_ShouldThrow_WhenRatiosSumToZero()
	{
		// Arrange
		var m = new Money(100, new Currency("EUR"));

		// Act
		object Fn() => m.Allocate(new decimal[] { 0, 0, 0 });

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void Allocate_ShouldThrow_WhenRatiosSumToNegative()
	{
		// Arrange
		var m = new Money(100, new Currency("EUR"));

		// Act
		object Fn() => m.Allocate(new decimal[] { -1, -2, -3 });

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void Allocate_ShouldThrow_WhenSomeRatioIsNegative()
	{
		// Arrange
		var m = new Money(100, new Currency("EUR"));

		// Act
		object Fn() => m.Allocate(new decimal[] { 1, 2, -1 });

		// Assert
		Assert.Throws<ArgumentException>((Func<object>)Fn);
	}

	[Theory]
	[MemberData(nameof(AllocateData))]
	public void Allocate(long amount, decimal[] ratios, long[] results)
	{
		// Arrange
		var m = new Money(amount, new Currency("EUR"));

		// Act
		var moneys = m.Allocate(ratios);
		var actual = moneys.Select((allocated) => allocated.Amount).ToArray();

		// Assert
		Assert.Equal(amount, actual.Sum());
		Assert.Equal(results, actual);
	}

	[Fact]
	public void AllocateTo_ShouldThrow_WhenGivenZero()
	{
		// Arrange
		var m = new Money(1000, new Currency("EUR"));

		// Act
		object Fn() => m.AllocateTo(0);

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Theory]
	[MemberData(nameof(AllocateToTargetsData))]
	public void AllocateTo(long amount, uint n, long[] results)
	{
		// Arrange
		var m = new Money(amount, new Currency("EUR"));

		// Act
		var a = m.AllocateTo(n);
		var actual = a.Select(allocated => allocated.Amount).ToArray();

		// Assert
		Assert.Equal(amount, actual.Sum());
		Assert.Equal(results, actual);
	}

	[Fact]
	public void RatioOf_ShouldThrow_WhenGivenZero()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(6, c);

		// Act
		object Fn() => b.RatioOf(a);

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Theory]
	[InlineData(0, 6, 0)]
	[InlineData(3, 6, 0.5)]
	[InlineData(3, 3, 1)]
	[InlineData(6, 3, 2)]
	public void RatioOf(long amountA, long amountB, decimal result)
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(amountA, c);
		var b = new Money(amountB, c);

		// Act
		var actual = a.RatioOf(b);

		// Assert
		Assert.Equal(result, actual);
	}

	[Theory]
	[InlineData(2, 6, 100)]
	[InlineData(6, 2, 100)]
	[InlineData(6, 10, 50)]
	[InlineData(10, 6, 50)]
	public void PercentageDifference(long amountA, long amountB, decimal result)
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(amountA, c);
		var b = new Money(amountB, c);

		// Act
		var actual = a.PercentageDifference(b);

		// Assert
		Assert.Equal(result, actual);
	}

	[Theory]
	[InlineData(5, 5, 0)]
	[InlineData(2, 8, 300)]
	[InlineData(8, 2, -75)]
	[InlineData(5, 10, 100)]
	[InlineData(10, 5, -50)]
	public void PercentageChange(long amountA, long amountB, decimal result)
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(amountA, c);
		var b = new Money(amountB, c);

		// Act
		var actual = a.PercentageChange(b);

		// Assert
		Assert.Equal(result, actual);
	}

	[Fact]
	public void IsZero_ShouldBeFalse_WhenGivenNegativeValue()
	{
		// Arrange
		var m = new Money(-10, new Currency("EUR"));

		// Act
		var result = m.IsZero();

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void IsZero_ShouldBeTrue_WhenGivenZero()
	{
		// Arrange
		var m = new Money(0, new Currency("EUR"));

		// Act
		var result = m.IsZero();

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void IsZero_ShouldBeFalse_WhenGivenPositiveValue()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var result = m.IsZero();

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void IsPositive_ShouldBeFalse_WhenGivenNegativeValue()
	{
		// Arrange
		var m = new Money(-10, new Currency("EUR"));

		// Act
		var result = m.IsPositive();

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void IsPositive_ShouldBeTrue_WhenGivenPositiveValue()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var result = m.IsPositive();

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void IsNegative_ShouldBeTrue_WhenGivenNegativeValue()
	{
		// Arrange
		var m = new Money(-10, new Currency("EUR"));

		// Act
		var result = m.IsNegative();

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void IsNegative_ShouldBeFalse_WhenGivenPositiveValue()
	{
		// Arrange
		var m = new Money(10, new Currency("EUR"));

		// Act
		var result = m.IsNegative();

		// Assert
		Assert.False(result);
	}

	[Theory]
	[MemberData(nameof(CastToDecimalData))]
	public void ToDecimal(string currency, long amount, byte minorUnits, decimal result)
	{
		// Arrange
		var m = new Money(
			amount,
			new Currency(currency, currency, minorUnits, 0, Currency.GenericCurrencySign)
		);

		// Act
		var d = m.ToDecimal();

		// Assert
		Assert.Equal(result, d);
	}

	[Fact]
	public void Min_ShouldThrow_WhenGivenEmptyCollection()
	{
		// Act
		object Fn() => Money.Min(Array.Empty<Money>());

		// Assert

		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void Min_ShouldThrow_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var collection = new[]
		{
			new Money(0, new Currency("EUR")),
			new Money(0, new Currency("USD")),
		};

		// Act
		object Fn() => Money.Min(collection);

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void Min()
	{
		// Arrange
		var c = new Currency("EUR");
		var collection = new[] { new Money(5, c), new Money(0, c), new Money(-5, c) };

		// Act
		var r = Money.Min(collection);

		// Assert
		Assert.Equal(-5, r.Amount);
	}

	[Fact]
	public void Min_WhenGivenTwoArguments()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5_00, c);
		var b = new Money(2_00, c);

		// Act
		var r = Money.Min(a, b);

		// Assert
		Assert.Equal(2_00, r.Amount);
	}

	[Fact]
	public void Max_ShouldThrow_WhenGivenEmptyCollection()
	{
		// Act
		object Fn() => Money.Max(Array.Empty<Money>());

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void Max_ShouldThrow_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var collection = new[]
		{
			new Money(0, new Currency("EUR")),
			new Money(0, new Currency("USD")),
		};

		// Act
		object Fn() => Money.Max(collection);

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void Max()
	{
		// Arrange
		var c = new Currency("EUR");
		var collection = new[] { new Money(-5, c), new Money(0, c), new Money(5, c) };

		// Act
		var r = Money.Max(collection);

		// Assert
		Assert.Equal(5, r.Amount);
	}

	[Fact]
	public void Max_WhenGivenTwoArguments()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5_00, c);
		var b = new Money(2_00, c);

		// Act
		var r = Money.Max(a, b);

		// Assert
		Assert.Equal(5_00, r.Amount);
	}

	[Fact]
	public void Sum_ShouldThrow_WhenGivenEmptyCollection()
	{
		// Act
		object Fn() => Money.Sum(Array.Empty<Money>());

		// Assert
		Assert.Throws<ArgumentException>(Fn);
	}

	[Fact]
	public void Sum()
	{
		// Arrange
		var c = new Currency("EUR");
		var collection = new[] { new Money(1, c), new Money(2, c), new Money(4, c) };

		// Act
		var r = Money.Sum(collection);

		// Assert
		Assert.Equal(7, r.Amount);
	}

	[Fact]
	public void Average_ShouldThrow_WhenGivenEmptyCollection()
	{
		// Act
		object Fn() => Money.Average(Array.Empty<Money>());

		// Assert
		var e = Assert.Throws<ArgumentException>(Fn);
		Assert.Equal("Called Average on an empty array", e.Message);
	}

	[Fact]
	public void Average()
	{
		// Arrange
		var c = new Currency("EUR");
		var collection = new[] { new Money(1, c), new Money(2, c), new Money(3, c) };

		// Act
		var r = Money.Average(collection);

		// Assert
		Assert.Equal(2, r.Amount);
	}

	[Fact]
	public void WeightedAverage_ShouldThrow_WhenGivenEmptyCollection()
	{
		// Act
		object Fn() => Money.WeightedAverage(Array.Empty<KeyValuePair<Money, decimal>>());

		// Assert
		var e = Assert.Throws<ArgumentException>(Fn);
		Assert.Equal("Called WeightedAverage on an empty collection", e.Message);
	}

	[Fact]
	public void WeightedAverage_ShouldThrow_WhenHavingDifferentCurrencies()
	{
		// Arrange
		var data = new KeyValuePair<Money, decimal>[]
		{
			new(new Money(0, new Currency("EUR")), 0.5M),
			new(new Money(0, new Currency("USD")), 0.5M),
		};

		// Act
		object Fn() => Money.WeightedAverage(data);

		// Assert
		var e = Assert.Throws<ArgumentException>(Fn);
		Assert.Equal("Currencies must be identical", e.Message);
	}

	[Fact]
	public void WeightedAverage_WhenGivenSingleValue()
	{
		// Arrange
		var price = new Money(100, new Currency("USD"));

		// Act
		var result = Money.WeightedAverage(new KeyValuePair<Money, decimal>[] { new(price, 0.5M) });

		// Assert
		Assert.Equal(price, result);
	}

	[Fact]
	public void WeightedAverage_WhenGivenSameValues()
	{
		// Arrange
		var currency = new Currency("USD");

		// Act
		var result = Money.WeightedAverage(
			new KeyValuePair<Money, decimal>[]
			{
				new(new Money(100, currency), 0.1M),
				new(new Money(100, currency), 0.1M),
				new(new Money(200, currency), 0.6M),
				new(new Money(200, currency), 0.2M),
			}
		);

		// Assert
		Assert.Equal(new Money(180, currency), result);
	}

	[Fact]
	public void WeightedAverage()
	{
		// Arrange
		var currency = new Currency("USD");

		// Act
		var result = Money.WeightedAverage(
			new KeyValuePair<Money, decimal>[]
			{
				new(new Money(100, currency), 0.1M),
				new(new Money(200, currency), 0.1M),
				new(new Money(300, currency), 0.7M),
				new(new Money(400, currency), 0.1M),
			}
		);

		// Assert
		Assert.Equal(new Money(280, currency), result);
	}

	[Fact]
	public void OperatorEquals_ShouldBeTrue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(0, c);

		// Act
		var result = a == b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorEquals_ShouldBeFalse()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(5, c);

		// Act
		var result = a == b;

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void OperatorEquals_ShouldBeTrue_WhenComparingNulls()
	{
		// Arrange
		Money? m = null;

		// Act
		var result = m == null;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorNotEquals_ShouldBeTrue_WhenGivenDifferentValues()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(5, c);

		// Act
		var result = a != b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorNotEquals_ShouldBeFalse_WhenGivenSameValues()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(0, c);

		// Act
		var result = a != b;

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void OperatorNotEquals_ShouldBeFalse_WhenComparingNulls()
	{
		// Arrange
		Money? m = null;

		// Act
		var result = m != null;

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void OperatorGreaterThan_ShouldBeTrue_WhenGreaterThan()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(0, c);

		// Act
		var result = a > b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorGreaterThan_ShouldBeFalse_WhenSmallerThan()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(5, c);

		// Act
		var result = a > b;

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void OperatorGreaterThanOrEqual_ShouldBeTrue_WhenGreaterThan()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(0, c);

		// Act
		var result = a >= b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorGreaterThanOrEqual_ShouldBeTrue_WhenGivenEqualValues()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(5, c);

		// Act
		var result = a >= b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorGreaterThanOrEqual_ShouldBeFalse_WhenGivenSmallerValue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(5, c);

		// Act
		var result = a >= b;

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void OperatorLessThan_ShouldBeTrue_WhenGivenSmallerValue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(5, c);

		// Act
		var result = a < b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorLessThan_ShouldBeFalse_WhenGivenLargerValue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(0, c);

		// Act
		var result = a < b;

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void OperatorLessThanOrEqual_ShouldBeTrue_WhenGivenSmallerValue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(0, c);
		var b = new Money(5, c);

		// Act
		var result = a <= b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorLessThanOrEqual_ShouldBeTrue_WhenGivenEqualValue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(5, c);

		// Act
		var result = a <= b;

		// Assert
		Assert.True(result);
	}

	[Fact]
	public void OperatorLessThanOrEqual_ShouldBeFalse_WhenGivenLargerValue()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(0, c);

		// Act
		var result = a <= b;

		// Assert
		Assert.False(result);
	}

	[Fact]
	public void OperatorAdd_WhenGivenMoneyInstance()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(10, c);

		// Act
		var r = a + b;

		// Assert
		Assert.Equal(15, r.Amount);
	}

	[Fact]
	public void OperatorAdd_WhenGivenDecimalOnLeft()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = 5M;
		var b = new Money(10_00, c);

		// Act
		var r = a + b;

		// Assert
		Assert.Equal(15_00, r.Amount);
	}

	[Fact]
	public void OperatorAdd_WhenGivenDecimalOnRight()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(10_00, c);
		var b = 5M;

		// Act
		var r = a + b;

		// Assert
		Assert.Equal(15_00, r.Amount);
	}

	[Fact]
	public void OperatorSubtract_WhenGivenMoneyInstance()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5, c);
		var b = new Money(2, c);

		// Act
		var r = a - b;

		// Assert
		Assert.Equal(3, r.Amount);
	}

	[Fact]
	public void OperatorSubtract_WhenGivenDecimalOnLeft()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = 5M;
		var b = new Money(2_00, c);

		// Act
		var r = a - b;

		// Assert
		Assert.Equal(3_00, r.Amount);
	}

	[Fact]
	public void OperatorSubtract_WhenGivenDecimalOnRight()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5_00, c);
		var b = 2M;

		// Act
		var r = a - b;

		// Assert
		Assert.Equal(3_00, r.Amount);
	}

	[Fact]
	public void OperatorMultiply_WhenGivenMoneyOnRight()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(5_00, c);
		var b = new Money(2_00, c);

		// Act
		var r = a * b;

		// Assert
		Assert.Equal(10_00, r.Amount);
	}

	[Fact]
	public void OperatorMultiply_WhenGivenDecimalOnRight()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(5, c);

		// Act
		var r = m * 2;

		// Assert
		Assert.Equal(10, r.Amount);
	}

	[Fact]
	public void OperatorMultiply_WhenGivenDecimalOnLeft()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(5, c);

		// Act
		var r = 2 * m;

		// Assert
		Assert.Equal(10, r.Amount);
	}

	[Fact]
	public void OperatorMultiply_WhenGivenDoubleOnRight()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(5, c);

		// Act
		var r = m * 2D;

		// Assert
		Assert.Equal(10, r.Amount);
	}

	[Fact]
	public void OperatorMultiply_WhenGivenDoubleOnLeft()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(5, c);

		// Act
		var r = 2D * m;

		// Assert
		Assert.Equal(10, r.Amount);
	}

	[Fact]
	public void OperatorMultiply_WhenGivenIntOnRight()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(5_00, c);

		// Act
		var r = m * 2;

		// Assert
		Assert.Equal(10_00, r.Amount);
	}

	[Fact]
	public void OperatorMultiply_WhenGivenIntOnLeft()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(5_00, c);

		// Act
		var r = 2 * m;

		// Assert
		Assert.Equal(10_00, r.Amount);
	}

	[Fact]
	public void OperatorDivide_WhenGivenMoney()
	{
		// Arrange
		var c = new Currency("EUR");
		var a = new Money(10_00, c);
		var b = new Money(2_00, c);

		// Act
		var r = a / b;

		// Assert
		Assert.Equal(5_00, r.Amount);
	}

	[Fact]
	public void OperatorDivide_WhenGivenDecimal()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(10, c);

		// Act
		var r = m / 2M;

		// Assert
		Assert.Equal(5, r.Amount);
	}

	[Fact]
	public void OperatorDivide_WhenGivenDouble()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(10_00, c);

		// Act
		var r = m / 2D;

		// Assert
		Assert.Equal(5_00, r.Amount);
	}

	[Fact]
	public void OperatorDivide_WhenGivenInt()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(10_00, c);

		// Act
		var r = m / 2;

		// Assert
		Assert.Equal(5_00, r.Amount);
	}

	[Fact]
	public void OperatorMod_WhenGivenDecimal()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(10, c);

		// Act
		var r = m % 3M;

		// Assert
		Assert.Equal(1, r.Amount);
	}

	[Fact]
	public void OperatorMod_WhenGivenDouble()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(10_00, c);

		// Act
		var r = m % 3D;

		// Assert
		Assert.Equal(1, r.Amount);
	}

	[Fact]
	public void OperatorMod_WhenGivenInt()
	{
		// Arrange
		var c = new Currency("EUR");
		var m = new Money(100_00, c);

		// Act
		var r = m % 3;

		// Assert
		Assert.Equal(1, r.Amount);
	}

	[Theory]
	[MemberData(nameof(CastToDecimalData))]
	public void OperatorCast_ToDecimal(
		string currency,
		long amount,
		byte minorUnits,
		decimal result
	)
	{
		// Arrange
		var m = new Money(
			amount,
			new Currency(currency, currency, minorUnits, 0, Currency.GenericCurrencySign)
		);

		// Act
		var d = (decimal)m;

		// Assert
		Assert.Equal(result, d);
	}

	public static IEnumerable<object[]> AllocateData()
	{
		yield return new object[] { 100L, new decimal[] { 1, 1, 1 }, new long[] { 34, 33, 33 } };
		yield return new object[] { 101L, new decimal[] { 1, 1, 1 }, new long[] { 34, 34, 33 } };
		yield return new object[] { 5L, new decimal[] { 3, 7 }, new long[] { 2, 3 } };
		yield return new object[] { 5L, new decimal[] { 7, 3 }, new long[] { 4, 1 } };
		yield return new object[] { 5L, new decimal[] { 7, 3, 0 }, new long[] { 4, 1, 0 } };
		yield return new object[] { -5L, new decimal[] { 7, 3 }, new long[] { -3, -2 } };
		yield return new object[] { 5L, new decimal[] { 0, 7, 3 }, new long[] { 0, 4, 1 } };
		yield return new object[] { 5L, new decimal[] { 7, 0, 3 }, new long[] { 4, 0, 1 } };
		yield return new object[] { 5L, new decimal[] { 0, 0, 1 }, new long[] { 0, 0, 5 } };
		yield return new object[] { 5L, new decimal[] { 0, 3, 7 }, new long[] { 0, 2, 3 } };
		yield return new object[] { 0L, new decimal[] { 0, 0, 1 }, new long[] { 0, 0, 0 } };
		yield return new object[] { 2L, new decimal[] { 1, 1, 1 }, new long[] { 1, 1, 0 } };
		yield return new object[] { 1L, new decimal[] { 1, 1 }, new long[] { 1, 0 } };
		yield return new object[] { 1L, new[] { 0.33M, 0.66M }, new long[] { 0, 1 } };
		yield return new object[] { 101L, new decimal[] { 3, 7 }, new long[] { 30, 71 } };
		yield return new object[] { 101L, new decimal[] { 7, 3 }, new long[] { 71, 30 } };
	}

	public static IEnumerable<object[]> AllocateToTargetsData()
	{
		yield return new object[] { 15L, 2U, new long[] { 8, 7 } };
		yield return new object[] { 10L, 2U, new long[] { 5, 5 } };
		yield return new object[] { 15L, 3U, new long[] { 5, 5, 5 } };
		yield return new object[] { 10L, 3U, new long[] { 4, 3, 3 } };
	}

	public static IEnumerable<object[]> CastToDecimalData()
	{
		yield return new object[] { "USD", 543L, 2, 5.43M };
		yield return new object[] { "JPY", 543L, 0, 543M };
		yield return new object[] { "ETH", 100000000000000000L, 18, 0.100000000000000000M };
	}
}
