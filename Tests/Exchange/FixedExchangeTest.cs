using DKX.NMoney;
using DKX.NMoney.Exchange;
using Xunit;

namespace DKX.NMoneyTests.Exchange;

public sealed class FixedExchangeTest
{
	[Fact]
	public async Task GetConversionRatio_ShouldReturnCorrectCorrectRate()
	{
		// Arrange
		var fromCurrency = new Currency("EUR");
		var toCurrency = new Currency("USD");
		var ex = new FixedExchange(new[] { new ExchangeRate(fromCurrency, toCurrency, 5) });

		// Act
		var rate = await ex.GetConversionRatio(fromCurrency, toCurrency);

		// Assert
		Assert.Equal(fromCurrency, rate.Source);
		Assert.Equal(toCurrency, rate.Target);
		Assert.Equal(5, rate.Rate);
	}

	[Fact]
	public async Task GetConversionRatio_ShouldThrow_WhenConversionRateDoesNotExists()
	{
		// Arrange
		var fromCurrency = new Currency("EUR");
		var toCurrency = new Currency("USD");
		var ex = new FixedExchange(Array.Empty<ExchangeRate>());

		// Act
		Task Fn() => ex.GetConversionRatio(fromCurrency, toCurrency);

		// Assert
		await Assert.ThrowsAsync<ArgumentException>(Fn);
	}
}
