using System.Net;
using System.Net.Http.Json;
using DKX.NMoney;
using DKX.NMoney.Exchange;
using NSubstitute;
using RichardSzalay.MockHttp;
using Xunit;

namespace DKX.NMoneyTests.Exchange;

public sealed class FixerExchangeTest
{
	private readonly MockHttpMessageHandler _httpHandler = new();

	private readonly IHttpClientFactory _httpClientFactory = Substitute.For<IHttpClientFactory>();

	private readonly FixerExchange _sut;

	public FixerExchangeTest()
	{
		// Arrange
		_httpClientFactory
			.CreateClient(FixerExchange.HttpClientName)
			.Returns(new HttpClient(_httpHandler));
		_sut = new FixerExchange(_httpClientFactory, "", false);
	}

	[Fact]
	public async Task GetConversionRatio_ShouldReturnExistingRate()
	{
		// Arrange
		var fromCurrency = new Currency("EUR");
		var toCurrency = new Currency("USD");
		_httpHandler
			.When("http://data.fixer.io/api/latest?access_key=&base=EUR&symbols=USD")
			.Respond(
				HttpStatusCode.OK,
				JsonContent.Create(
					new
					{
						success = true,
						timestamp = 1582536008,
						@base = "EUR",
						date = "2020-02-24",
						rates = new { USD = 1.082872 },
					}
				)
			);

		// Act
		var rate = await _sut.GetConversionRatio(fromCurrency, toCurrency);

		// Assert
		Assert.Equal(fromCurrency, rate.Source);
		Assert.Equal(toCurrency, rate.Target);
		Assert.Equal(1.082872M, rate.Rate);
	}

	[Fact]
	public async Task GetConversionRatio_ShouldThrow_WhenGivenUnknownCurrencies()
	{
		// Arrange
		var fromCurrency = new Currency("XXX", "XXX", 0, 0, Currency.GenericCurrencySign);
		var toCurrency = new Currency("YYY", "YYY", 0, 0, Currency.GenericCurrencySign);
		_httpHandler
			.When("http://data.fixer.io/api/latest?access_key=&base=XXX&symbols=YYY")
			.Respond(
				HttpStatusCode.OK,
				JsonContent.Create(
					new
					{
						success = false,
						error = new { code = 201, type = "invalid_base_currency" },
					}
				)
			);

		// Act
		Task Fn() => _sut.GetConversionRatio(fromCurrency, toCurrency);

		// Assert
		await Assert.ThrowsAsync<ArgumentException>(Fn);
	}
}
