using DKX.NMoney.CurrencyList;
using Xunit;

namespace DKX.NMoneyTests.CurrencyList;

public sealed class CryptoCurrencyListTest
{
	private readonly CryptoCurrencyList _sut = CryptoCurrencyList.Instance;

	[Fact]
	public void GetByCode_ShouldThrow_WhenUnknownCurrency()
	{
		// Act
		object Fn() => _sut.GetByCode("???");

		// Assert
		Assert.Throws<ArgumentOutOfRangeException>(Fn);
	}

	[Fact]
	public void GetByCode_ShouldReturnBitcoin()
	{
		// Act
		var c = _sut.GetByCode("XBT");

		// Assert
		Assert.Equal("XBT", c.Code);
		Assert.Equal("Bitcoin", c.Name);
		Assert.Equal(8, c.MinorUnit);
		Assert.Equal(0, c.NumericCode);
	}
}
