using DKX.NMoney.CurrencyList;
using Xunit;

namespace DKX.NMoneyTests.CurrencyList;

public sealed class IsoCurrencyListTest
{
	private readonly IsoCurrencyList _sut = IsoCurrencyList.Instance;

	[Fact]
	public void GetByCode_ShouldThrow_WhenUnknownCurrency()
	{
		// Act
		object Fn() => _sut.GetByCode("???");

		// Assert
		Assert.Throws<ArgumentOutOfRangeException>(Fn);
	}

	[Fact]
	public void GetByCode_ShouldReturnEuro()
	{
		// Act
		var c = _sut.GetByCode("EUR");

		// Assert
		Assert.Equal("EUR", c.Code);
		Assert.Equal("Euro", c.Name);
		Assert.Equal(2, c.MinorUnit);
		Assert.Equal(978, c.NumericCode);
	}
}
