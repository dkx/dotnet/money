# JSON (de)serialization

`Money` object can be converted to or from json with provided `MoneyConverter`.

There are currently two converters and they differ by their [currencies list](./currencies.md).

* `IsoMoneyConverter`
* `CryptoMoneyConverter`

**Example of supported json:**

```json
{
    "currency": "EUR",
    "amount": "500"
}
```

Reason for using strings instead of numeric types in place of `amount` is that you may be sending or receiving money 
values from systems without native support for `long/int64`. Sending string will make sure that there is same value on 
both ends.

## Setup

```c#
using DKX.NMoney.Json;
using System.Text.Json;
using System.Text.Json.Serialization

// mapped class
class Data
{
    [JsonPropertyName("price")]
    public Money Price { get; set; } = default!;
}

// json options
var options = new JsonSerializerOptions();
options.Converters.Add(new MoneyConverter(IsoCurrencyList.Instance));
```

## Serialization

```c#
using DKX.NMoney.Json;
using System.Text.Json;

var data = new Data
{
    Price = new Money(5_00, "EUR"),     // €5.00
};

var json = JsonSerializer.Serialize(data, options);
```

**output:**

```json
{
    "price": {
        "currency": "EUR",
        "amount": "500"
    }
}
```

## Deserialization

**input:**

```json
{
    "price": {
        "currency": "USD",
        "amount": "250"
    }
}
```

```c#
using DKX.NMoney.Json;
using System.Text.Json;

var data = JsonSerializer.Deserialize<Data>(json, _options);
var price = data.Price;     // $2.50

Console.WriteLine(price.Currency.Code);     // output: USD
Console.WriteLine(price.Amount);            // output: 250
```
