# Formatting

## Culture formatter

`CultureFormatter` outputs a string based on provided culture info.

```c#
using DKX.NMoney;
using DKX.NMoney.Formatter;

var cultureInfo = new CultureInfo("en-US");
var numberFormat = cultureInfo.NumberFormat;

var money = new Money(1_00, "USD");    // $1.00
var formatter = new CultureFormatter(numberFormat);

formatter.Format(money);    // outputs: $1.00
```

## Decimal formatter

This formatter outputs a simple decimal string which is always in a consistent format independent of locale.

```c#
using DKX.NMoney;
using DKX.NMoney.Formatter;

var money = new Money(1_00, "USD");    // $1.00
var formatter = new DecimalFormatter();

formatter.Format(money);    // outputs: 1.00
```
