# Currency conversion

To convert a `Money` instance from one `Currency` to another, you need the `Converter`. This class depends on
`IExchange`. `IExchange` returns a `ExchangeRate`, which is the combination of the base currency, counter currency and 
the conversion ratio.

## Fixed conversion

You can use a fixed exchange to convert `Money` into another `Currency`.

```c#
using DKX.NMoney;
using DKX.NMoney.Exchange;

var eur = new Currency("EUR");
var usd = new Currency("USD");

var exchange = new FixedExchange(new [] {
    new ExchangeRate(usd, eur, 1.25M),
});

var converter = new Converter(exchange);

var eur100 = new Money(1_00, eur);                    // €1.00
var usd125 = await converter.Convert(eur100, usd);    // $1.25
```

## Fixer.io

Exchange rates from [fixer.io](https://fixer.io/).

You have to [register a HTTP client factory](https://learn.microsoft.com/en-us/dotnet/core/extensions/httpclient-factory) with name `FixerExchange`.

```c#
using DKX.NMoney;
using DKX.NMoney.Exchange;
using System.Net.Http;

var useHttps = true;

var exchange = new FixerExchange(httpClientFactory, "YOUR_FIXER_API_KEY", useHttps);
var converter = new Converter(exchange);

var eur = new Money(1_00, new Currency("EUR"));                // €1.00
var usd = await converter.Convert(eur, new Currency("USD"));   // $?.??
```
