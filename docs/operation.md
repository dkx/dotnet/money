# Operations

## Addition

```c#
using DKX.NMoney;

var value1 = new Money(8_00, "EUR");    // €8.00
var value2 = new Money(5_00, "EUR");    // €5.00

var result = value1.Add(value2);    // €13.00
// or simply
var result = value1 + value2;    // €13.00
```

## Subtraction

```c#
using DKX.NMoney;

var value1 = new Money(8_00, "EUR");    // €8.00
var value2 = new Money(5_00, "EUR");    // €5.00

var result = value1.Subtract(value2);    // €3.00
// or simply
var result = value1 - value2;    // €3.00
```

## Multiplication

```c#
using DKX.NMoney;

var value = new Money(8_00, "EUR");    // €8.00

var result = value.Multiply(2);    // €16.00
// or simply
var result = value * 2;    // €16.00
```

## Division

```c#
using DKX.NMoney;

var value = new Money(8_00, "EUR");    // €8.00

var result = value.Divide(2);    // €4.00
// or simply
var result = value / 2;    // €4.00
```

## Modulus

```c#
using DKX.NMoney;

var value = new Money(8_30, "EUR");    // €8.30

var result = value.Mod(3);    // €2.30
// or simply
var result = value % 3;    // €2.30
```

## Absolute value

```c#
using DKX.NMoney;

var value = new Money(-8_00, "EUR");    // -€8.00

var result = value.Abs();    // €8.00
```

## Negative value

```c#
using DKX.NMoney;

var value = new Money(8_00, "EUR");    // €8.00

var result = value.Negative();    // -€8.00
```

## Ratio of

`RatioOf()` provides the ratio of a Money object in comparison to another Money object.

```c#
using DKX.NMoney;

var three = new Money(3_00, "EUR");      // €3.00
var six = new Money(6_00, "EUR");        // €6.00

var result = three.RatioOf(six);        // 0.5
var result = six.RatioOf(three);        // 2
```

## Percentage difference

```c#
using DKX.NMoney;

var two = new Money(2_00, "EUR");        // €2.00
var six = new Money(6_00, "EUR");        // €6.00

var result = two.PercentageDifference(six);          // 100
var result = six.PercentageDifference(two);          // 100
```

## Percentage change

```c#
using DKX.NMoney;

var five = new Money(5_00, "EUR");        // €5.00
var ten = new Money(10_00, "EUR");        // €10.00

var result = five.PercentageChange(ten);         // 100
var result = ten.PercentageChange(five);         // -50
```
