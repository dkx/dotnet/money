# Comparison

## Same currency

```c#
using DKX.NMoney;

var value1 = new Money(8_00, "USD");    // $8.00
var value2 = new Money(1_00, "USD");    // $1.00
var value3 = new Money(8_00, "EUR");    // €8.00

var result = value1.IsSameCurrency(value2);    // true
var result = value1.IsSameCurrency(value3);    // false
```

## Equality

```c#
using DKX.NMoney;

var value1 = new Money(8_00, "USD");    // $8.00
var value2 = new Money(8_00, "USD");    // $8.00
var value3 = new Money(8_00, "EUR");    // €8.00

var result = value1.Equals(value2);    // true
var result = value1.Equals(value3);    // false
// or simply
var result == value1 == value2;    // true
var result == value1 == value3;    // false
```

## Greater than

```c#
using DKX.NMoney;

var value1 = new Money(8_00, "USD");    // $8.00
var value2 = new Money(7_00, "USD");    // $7.00

var result = value1.GreaterThan(value2);    // true
// or simply
var result == value1 > value2;    // true
```

You can also use `GreaterThanOrEqual()` to additionally check for equality.

```c#
using DKX.NMoney;

var value1 = new Money(8_00, "USD");    // $8.00
var value2 = new Money(8_00, "USD");    // $8.00

var result = value1.GreaterThanOrEqual(value2);    // true
// or simply
var result == value1 >= value2;    // true
```

## Less than

```c#
using DKX.NMoney;

var value1 = new Money(8_00, "USD");    // $8.00
var value2 = new Money(7_00, "USD");    // $7.00

var result = value1.LessThan(value2);    // false
// or simply
var result == value1 < value2;    // false
```

You can also use `LessThanOrEqual()` to additionally check for equality.

```c#
using DKX.NMoney;

var value1 = new Money(8_00, "USD");    // $8.00
var value2 = new Money(8_00, "USD");    // $8.00

var result = value1.LessThanOrEqual(value2);    // false
// or simply
var result == value1 <= value2;    // false
```

## Value sign

You may determine the sign of Money object using the following methods.

* `IsZero()`
* `IsPositive()`
* `IsNegative()`

```c#
using DKX.NMoney;

new Money(1_00, "USD").IsZero()        // false
new Money(0, "USD").IsZero()          // true
new Money(-1_00, "USD").IsZero()       // false

new Money(1_00, "USD").IsPositive()    // true
new Money(0, "USD").IsPositive()      // false
new Money(-1_00, "USD").IsPositive()   // false

new Money(1_00, "USD").IsNegative()    // false
new Money(0, "USD").IsNegative()      // false
new Money(-1_00, "USD").IsNegative()   // true
```
