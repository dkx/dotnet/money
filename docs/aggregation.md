# Aggregation

`Min()` returns the smallest of the given Money objects

```c#
using DKX.NMoney;

var collection = new[]
{
    new Money(1_00, "EUR"),     // €1.00
    new Money(2_00, "EUR"),     // €2.00
    new Money(3_00, "EUR"),     // €3.00
};

var min = Money.Min(collection);    // €1.00
```

`Max()` returns the largest of the given Money objects

```c#
using DKX.NMoney;

var collection = new[]
{
    new Money(1_00, "EUR"),     // €1.00
    new Money(2_00, "EUR"),     // €2.00
    new Money(3_00, "EUR"),     // €3.00
};

var min = Money.Max(collection);    // €3.00
```

`Average()` returns the average of the given Money objects as a Money object

```c#
using DKX.NMoney;

var collection = new[]
{
    new Money(1_00, "EUR"),     // €1.00
    new Money(-2_00, "EUR"),    // -€2.00
    new Money(3_00, "EUR"),     // €3.00
};

var avg = Money.Average(collection);    // €2.00
```

`WeightedAverage()` returns the weighted average of the given Money objects and weights as a Money object

> **Warning**
> Don't use Dictionary types as they don't allow equal keys.

```c#
using DKX.NMoney;

var collection = new KeyValuePair<Money, decimal>[]
{
    new(new Money(1_00, "EUR"), 0.1M),     // €1.00
    new(new Money(2_00, "EUR"), 0.1M),     // €2.00
    new(new Money(3_00, "EUR"), 0.7M),     // €3.00
    new(new Money(4_00, "EUR"), 0.1M),     // €4.00
};

var avg = Money.WeightedAverage(collection);    // €2.80
```

`Sum()` providers the sum of all given Money objects

```c#
using DKX.NMoney;

var collection = new[]
{
    new Money(1_00, "EUR"),     // €1.00
    new Money(-2_00, "EUR"),    // -€2.00
    new Money(3_00, "EUR"),     // €3.00
};

var sum = Money.Sum(collection);    // €2.00
```
