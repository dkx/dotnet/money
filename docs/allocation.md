# Allocation

## Allocate by ratios

Sometimes you need to split monetary values but percentages can't cut it without adding or losing pennies. A good
example is invoicing: let's say you need to bill $1,000.03 and you want a 50% downpayment. If you use percentage, you'll
get an accurate `Money` object but the amount won't be billable: you can't split a penny. If you round it, you'll bill a
penny extra. With allocate, you can split a monetary amount then distribute the remainder as evenly as possible.

```c#
using DKX.NMoney;

var money = new Money(1_01, "USD");   // $1.01
var allocated = money.Allocate(new [] { 1, 1, 1 });

var allocatedToArray = allocated.ToArray();

allocatedToArray[0].Amount;    // $0.34
allocatedToArray[1].Amount;    // $0.34
allocatedToArray[2].Amount;    // $0.33
```

## Allocate to N targets

```c#
using DKX.NMoney;

var money = new Money(1_01, "USD");   // $1.01
var allocated = money.AllocateTo(3);

var allocatedToArray = allocated.ToArray();

allocatedToArray[0].Amount;    // $0.34
allocatedToArray[1].Amount;    // $0.34
allocatedToArray[2].Amount;    // $0.33
```
