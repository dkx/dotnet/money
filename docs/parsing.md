# Parsing

## Decimal parser

This parser takes a simple decimal string which is always in a consistent format independent of locale.

```c#
using DKX.NMoney;
using DKX.NMoney.Parser;

var parser = new DecimalParser();
var money = parser.Parse("1000", new Currency("USD"));   // $1000.00

money.Amount;   // outputs: 100000
```
