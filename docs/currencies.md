# Currencies

Currencies lists are holders for different currencies. 

## ISO currencies

As it’s name says, the ISO currencies implementation provides all available ISO4217 currencies. It uses the official ISO
4217 Maintenance Agency as source for the data.

This currencies list is used by default.

```c#
using DKX.NMoney;

var money = new Money(1_00, "EUR");    // €1.00
```

example above is a shortcut for:

```c#
using DKX.NMoney;

var currency = new Currency("EUR");
var money = new Money(1_00, currency);    // €1.00
```

which is a shortcut for:

```c#
using DKX.NMoney;
using DKX.NMoney.CurrencyList;

var currencies = IsoCurrencyList.Instance;
var euro = currencies.GetByCode("EUR");
var money = new Money(1_00, euro);    // €1.00
```

## Crypto currencies list

Right now there is only Bitcoin provided by default.

```c#
using DKX.NMoney;
using DKX.NMoney.CurrencyList;

var currencies = CryptoCurrencyList.Instance;
var bitcoin = currencies.GetByCode("XBT");
var money = new Money(1_09384345, bitcoin);    // B1.09384345
```

# Custom currency

```c#
using DKX.NMoney;

var myCurrency = new Currency("AAA", "OoooOOOoooO", 3, 0, "£");
```

* `AAA`: Code of currency
* `OoooOOOoooO`: "Nice" name of currency
* `3`: Number of minor units
* `0`: Code of currency (applicable for ISO currencies)
* `£`: Symbol of currency - used in [formatting](./formatting.md)

## Custom currency list

See the source code to learn how to implement custom currency list.
