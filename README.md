# DKX/Money

Immutable & zero dependency .NET Core implementation of Fowler's Money pattern.

Inspired by [moneyphp](http://moneyphp.org/en/stable/).

## Installation

```bash
$ dotnet add package DKX.Money
```

## Basic usage

All amounts are represented in the smallest unit (eg. cents), so USD 5.00 is written as

```c#
using DKX.NMoney;

var fiver = new Money(500, "USD");
fiver.Amount;   // 500
```

`Money` object can be created from `decimal` type too, but it will be converted into the smallest units in constructor

```c#
using DKX.NMoney;

var fiver = new Money(5.05M, "USD");
fiver.Amount;   // 505
```

## Features

* [Operation](./docs/operation.md)
* [Comparison](./docs/comparison.md)
* [Aggregation](./docs/aggregation.md)
* [Allocation](./docs/allocation.md)
* [Parsing](./docs/parsing.md)
* [Formatting](./docs/formatting.md)
* [Currencies](./docs/currencies.md)
* [JSON conversion](./docs/json.md)
* [Currency conversion](./docs/currency-conversion.md)
