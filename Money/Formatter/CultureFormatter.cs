using System.Globalization;

namespace DKX.NMoney.Formatter;

public sealed class CultureFormatter : IFormatter
{
	private readonly NumberFormatInfo _numberInfo;

	public CultureFormatter(NumberFormatInfo numberFormat)
	{
		_numberInfo = numberFormat;
	}

	public string Format(Money money)
	{
		return Format(money, money.Currency.MinorUnit);
	}

	public string Format(Money money, byte minorUnits)
	{
		var numberInfo = (NumberFormatInfo)_numberInfo.Clone();
		numberInfo.CurrencySymbol = money.Currency.Symbol;
		numberInfo.CurrencyDecimalDigits = minorUnits;

		var value = Convert.ToDecimal(money.Amount);
		if (money.Currency.MinorUnit > 0)
		{
			value = decimal.Divide(value, Convert.ToInt64(Math.Pow(10, money.Currency.MinorUnit)));
		}

		return value.ToString("c", numberInfo);
	}
}
