namespace DKX.NMoney.Formatter;

public sealed class DecimalFormatter : IFormatter
{
	public string Format(Money money)
	{
		return Format(money, money.Currency.MinorUnit);
	}

	public string Format(Money money, byte minorUnits)
	{
		var valueBase = money.Abs().Amount.ToString();
		var valueLength = valueBase.Length;

		string formatted;

		if (valueLength > minorUnits)
		{
			formatted = valueBase.Substring(0, valueLength - minorUnits);
			var decimalDigits = valueBase.Substring(valueLength - minorUnits);

			if (decimalDigits.Length > 0)
			{
				formatted += $".{decimalDigits}";
			}
		}
		else
		{
			formatted = "0." + "".PadLeft(minorUnits - valueLength, '0') + valueBase;
		}

		if (money.IsNegative())
		{
			formatted = "-" + formatted;
		}

		return formatted;
	}
}
