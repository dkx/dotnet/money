namespace DKX.NMoney;

public sealed record ExchangeRate(Currency Source, Currency Target, decimal Rate);
