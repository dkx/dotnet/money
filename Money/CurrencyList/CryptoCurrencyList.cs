namespace DKX.NMoney.CurrencyList;

public sealed class CryptoCurrencyList : ICurrencyList
{
	public static readonly CryptoCurrencyList Instance = new();

	private readonly IReadOnlyDictionary<string, Currency> _currencies = new Dictionary<
		string,
		Currency
	>
	{
		["XBT"] = new("XBT", "Bitcoin", 8, 0, '\u20BF'.ToString()),
	};

	public Currency GetByCode(string code)
	{
		if (_currencies.TryGetValue(code, out var currency))
		{
			return currency;
		}

		throw new ArgumentOutOfRangeException(
			nameof(code),
			$"Currency '{code}' is not registered in {GetType().FullName}"
		);
	}
}
