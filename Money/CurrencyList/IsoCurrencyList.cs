namespace DKX.NMoney.CurrencyList;

public sealed class IsoCurrencyList : ICurrencyList
{
	public static readonly IsoCurrencyList Instance = new();

	private readonly IReadOnlyDictionary<string, Currency> _currencies = new Dictionary<
		string,
		Currency
	>
	{
		["AED"] = new("AED", "UAE Dirham", 2, 784, "د.إ"),
		["AFN"] = new("AFN", "Afghani", 2, 971, "؋"),
		["ALL"] = new("ALL", "Lek", 2, 8, "L"),
		["AMD"] = new("AMD", "Armenian Dram", 2, 51, "֏"),
		["ANG"] = new("ANG", "Netherlands Antillean Guilder", 2, 532, "ƒ"),
		["AOA"] = new("AOA", "Kwanza", 2, 973, "Kz"),
		["ARS"] = new("ARS", "Argentine Peso", 2, 32, "$"),
		["AUD"] = new("AUD", "Australian Dollar", 2, 36, "$"),
		["AWG"] = new("AWG", "Aruban Florin", 2, 533, "ƒ"),
		["AZN"] = new("AZN", "Azerbaijan Manat", 2, 944, "ман"),
		["BAM"] = new("BAM", "Convertible Mark", 2, 977, "KM"),
		["BBD"] = new("BBD", "Barbados Dollar", 2, 52, "$"),
		["BDT"] = new("BDT", "Taka", 2, 50, "৳"),
		["BGN"] = new("BGN", "Bulgarian Lev", 2, 975, "лв."),
		["BHD"] = new("BHD", "Bahraini Dinar", 3, 48, "BD"),
		["BIF"] = new("BIF", "Burundi Franc", 0, 108, "FBu"),
		["BMD"] = new("BMD", "Bermudian Dollar", 2, 60, "$"),
		["BND"] = new("BND", "Brunei Dollar", 2, 96, "$"),
		["BOB"] = new("BOB", "Boliviano", 2, 68, "Bs."),
		["BOV"] = new("BOV", "Mvdol", 2, 984, Currency.GenericCurrencySign),
		["BRL"] = new("BRL", "Brazilian Real", 2, 986, "R$"),
		["BSD"] = new("BSD", "Bahamian Dollar", 2, 44, "$"),
		["BTN"] = new("BTN", "Ngultrum", 2, 64, "Nu."),
		["BWP"] = new("BWP", "Pula", 2, 72, "P"),
		["BYN"] = new("BYN", "Belarusian Ruble", 2, 933, "Br"),
		["BZD"] = new("BZD", "Belize Dollar", 2, 84, "BZ$"),
		["CAD"] = new("CAD", "Canadian Dollar", 2, 124, "$"),
		["CDF"] = new("CDF", "Congolese Franc", 2, 976, "FC"),
		["CHE"] = new("CHE", "WIR Euro", 2, 947, "CHE"),
		["CHF"] = new("CHF", "Swiss Franc", 2, 756, "fr."),
		["CHW"] = new("CHW", "WIR Franc", 2, 948, "CHW"),
		["CLF"] = new("CLF", "Unidad de Fomento", 4, 990, "CLF"),
		["CLP"] = new("CLP", "Chilean Peso", 0, 152, "$"),
		["CNY"] = new("CNY", "Yuan Renminbi", 2, 156, "¥"),
		["COP"] = new("COP", "Colombian Peso", 2, 170, "$"),
		["COU"] = new("COU", "Unidad de Valor Real", 2, 970, Currency.GenericCurrencySign),
		["CRC"] = new("CRC", "Costa Rican Colon", 2, 188, "₡"),
		["CUC"] = new("CUC", "Peso Convertible", 2, 931, "CUC$"),
		["CUP"] = new("CUP", "Cuban Peso", 2, 192, "$"),
		["CVE"] = new("CVE", "Cabo Verde Escudo", 2, 132, "$"),
		["CZK"] = new("CZK", "Czech Koruna", 2, 203, "Kč"),
		["DJF"] = new("DJF", "Djibouti Franc", 0, 262, "Fdj"),
		["DKK"] = new("DKK", "Danish Krone", 2, 208, "kr."),
		["DOP"] = new("DOP", "Dominican Peso", 2, 214, "RD$"),
		["DZD"] = new("DZD", "Algerian Dinar", 2, 12, "DA"),
		["EGP"] = new("EGP", "Egyptian Pound", 2, 818, "LE"),
		["ERN"] = new("ERN", "Nakfa", 2, 232, "ERN"),
		["ETB"] = new("ETB", "Ethiopian Birr", 2, 230, "Br"),
		["EUR"] = new("EUR", "Euro", 2, 978, "€"),
		["FJD"] = new("FJD", "Fiji Dollar", 2, 242, "$"),
		["FKP"] = new("FKP", "Falkland Islands Pound", 2, 238, "£"),
		["GBP"] = new("GBP", "Pound Sterling", 2, 826, "£"),
		["GEL"] = new("GEL", "Lari", 2, 981, "ლ."),
		["GHS"] = new("GHS", "Ghana Cedi", 2, 936, "GH¢"),
		["GIP"] = new("GIP", "Gibraltar Pound", 2, 292, "£"),
		["GMD"] = new("GMD", "Dalasi", 2, 270, "D"),
		["GNF"] = new("GNF", "Guinean Franc", 0, 324, "FG"),
		["GTQ"] = new("GTQ", "Quetzal", 2, 320, "Q"),
		["GYD"] = new("GYD", "Guyana Dollar", 2, 328, "$"),
		["HKD"] = new("HKD", "Hong Kong Dollar", 2, 344, "HK$"),
		["HNL"] = new("HNL", "Lempira", 2, 340, "L"),
		["HRK"] = new("HRK", "Kuna", 2, 191, "kn"),
		["HTG"] = new("HTG", "Gourde", 2, 332, "G"),
		["HUF"] = new("HUF", "Forint", 2, 348, "Ft"),
		["IDR"] = new("IDR", "Rupiah", 2, 360, "Rp"),
		["ILS"] = new("ILS", "New Israeli Sheqel", 2, 376, "₪"),
		["INR"] = new("INR", "Indian Rupee", 2, 356, "₹"),
		["IQD"] = new("IQD", "Iraqi Dinar", 3, 368, "د.ع"),
		["IRR"] = new("IRR", "Iranian Rial", 2, 364, "ريال"),
		["ISK"] = new("ISK", "Iceland Krona", 0, 352, "kr"),
		["JMD"] = new("JMD", "Jamaican Dollar", 2, 388, "J$"),
		["JOD"] = new("JOD", "Jordanian Dinar", 3, 400, "د.ا.‏"),
		["JPY"] = new("JPY", "Yen", 0, 392, "¥"),
		["KES"] = new("KES", "Kenyan Shilling", 2, 404, "KSh"),
		["KGS"] = new("KGS", "Som", 2, 417, "сом"),
		["KHR"] = new("KHR", "Riel", 2, 116, "៛"),
		["KMF"] = new("KMF", "Comorian Franc ", 0, 174, "CF"),
		["KPW"] = new("KPW", "North Korean Won", 2, 408, "₩"),
		["KRW"] = new("KRW", "Won", 0, 410, "₩"),
		["KWD"] = new("KWD", "Kuwaiti Dinar", 3, 414, "د.ك"),
		["KYD"] = new("KYD", "Cayman Islands Dollar", 2, 136, "$"),
		["KZT"] = new("KZT", "Tenge", 2, 398, "₸"),
		["LAK"] = new("LAK", "Lao Kip", 2, 418, "₭"),
		["LBP"] = new("LBP", "Lebanese Pound", 2, 422, "ل.ل"),
		["LKR"] = new("LKR", "Sri Lanka Rupee", 2, 144, "Rs"),
		["LRD"] = new("LRD", "Liberian Dollar", 2, 430, "$"),
		["LSL"] = new("LSL", "Loti", 2, 426, "L"),
		["LYD"] = new("LYD", "Libyan Dinar", 3, 434, "ل.د"),
		["MAD"] = new("MAD", "Moroccan Dirham", 2, 504, "د.م."),
		["MDL"] = new("MDL", "Moldovan Leu", 2, 498, "L"),
		["MGA"] = new("MGA", "Malagasy Ariary", 2, 969, "Ar"),
		["MKD"] = new("MKD", "Denar", 2, 807, "ден"),
		["MMK"] = new("MMK", "Kyat", 2, 104, "K"),
		["MNT"] = new("MNT", "Tugrik", 2, 496, "₮"),
		["MOP"] = new("MOP", "Pataca", 2, 446, "MOP$"),
		["MRU"] = new("MRU", "Ouguiya", 2, 929, "UM"),
		["MUR"] = new("MUR", "Mauritius Rupee", 2, 480, "Rs"),
		["MVR"] = new("MVR", "Rufiyaa", 2, 462, "Rf"),
		["MWK"] = new("MWK", "Malawi Kwacha", 2, 454, "MK"),
		["MXN"] = new("MXN", "Mexican Peso", 2, 484, "$"),
		["MXV"] = new(
			"MXV",
			"Mexican Unidad de Inversion (UDI)",
			2,
			979,
			Currency.GenericCurrencySign
		),
		["MYR"] = new("MYR", "Malaysian Ringgit", 2, 458, "RM"),
		["MZN"] = new("MZN", "Mozambique Metical", 2, 943, "MTn"),
		["NAD"] = new("NAD", "Namibia Dollar", 2, 516, "N$"),
		["NGN"] = new("NGN", "Naira", 2, 566, "₦"),
		["NIO"] = new("NIO", "Cordoba Oro", 2, 558, "C$"),
		["NOK"] = new("NOK", "Norwegian Krone", 2, 578, "kr"),
		["NPR"] = new("NPR", "Nepalese Rupee", 2, 524, "Rs"),
		["NZD"] = new("NZD", "New Zealand Dollar", 2, 554, "$"),
		["OMR"] = new("OMR", "Rial Omani", 3, 512, "ر.ع."),
		["PAB"] = new("PAB", "Balboa", 2, 590, "B/."),
		["PEN"] = new("PEN", "Sol", 2, 604, "S/."),
		["PGK"] = new("PGK", "Kina", 2, 598, "K"),
		["PHP"] = new("PHP", "Philippine Peso", 2, 608, "₱"),
		["PKR"] = new("PKR", "Pakistan Rupee", 2, 586, "Rs"),
		["PLN"] = new("PLN", "Zloty", 2, 985, "zł"),
		["PYG"] = new("PYG", "Guarani", 0, 600, "₲"),
		["QAR"] = new("QAR", "Qatari Rial", 2, 634, "ر.ق"),
		["RON"] = new("RON", "Romanian Leu", 2, 946, "lei"),
		["RSD"] = new("RSD", "Serbian Dinar", 2, 941, "РСД"),
		["RUB"] = new("RUB", "Russian Ruble", 2, 643, "₽"),
		["RWF"] = new("RWF", "Rwanda Franc", 0, 646, "RFw"),
		["SAR"] = new("SAR", "Saudi Riyal", 2, 682, "ر.س"),
		["SBD"] = new("SBD", "Solomon Islands Dollar", 2, 90, "SI$"),
		["SCR"] = new("SCR", "Seychelles Rupee", 2, 690, "SR"),
		["SDG"] = new("SDG", "Sudanese Pound", 2, 938, "ج.س."),
		["SEK"] = new("SEK", "Swedish Krona", 2, 752, "kr"),
		["SGD"] = new("SGD", "Singapore Dollar", 2, 702, "S$"),
		["SHP"] = new("SHP", "Saint Helena Pound", 2, 654, "£"),
		["SLL"] = new("SLL", "Leone", 2, 694, "Le"),
		["SOS"] = new("SOS", "Somali Shilling", 2, 706, "S"),
		["SRD"] = new("SRD", "Surinam Dollar", 2, 968, "$"),
		["SSP"] = new("SSP", "South Sudanese Pound", 2, 728, "£"),
		["STN"] = new("STN", "Dobra", 2, 930, "Db"),
		["SVC"] = new("SVC", "El Salvador Colon", 2, 222, "₡"),
		["SYP"] = new("SYP", "Syrian Pound", 2, 760, "ܠ.ܣ.‏"),
		["SZL"] = new("SZL", "Lilangeni", 2, 748, "L"),
		["THB"] = new("THB", "Baht", 2, 764, "฿"),
		["TJS"] = new("TJS", "Somoni", 2, 972, "смн"),
		["TMT"] = new("TMT", "Turkmenistan New Manat", 2, 934, "m"),
		["TND"] = new("TND", "Tunisian Dinar", 3, 788, "د.ت"),
		["TOP"] = new("TOP", "Pa’anga", 2, 776, "T$"),
		["TRY"] = new("TRY", "Turkish Lira", 2, 949, "₺"),
		["TTD"] = new("TTD", "Trinidad and Tobago Dollar", 2, 780, "$"),
		["TWD"] = new("TWD", "New Taiwan Dollar", 2, 901, "NT$"),
		["TZS"] = new("TZS", "Tanzanian Shilling", 2, 834, "x/y"),
		["UAH"] = new("UAH", "Hryvnia", 2, 980, "₴"),
		["UGX"] = new("UGX", "Uganda Shilling", 0, 800, "USh"),
		["USD"] = new("USD", "US Dollar", 2, 840, "$"),
		["USN"] = new("USN", "US Dollar (Next day)", 2, 997, "$"),
		["UYI"] = new(
			"UYI",
			"Uruguay Peso en Unidades Indexadas (UI)",
			0,
			940,
			Currency.GenericCurrencySign
		),
		["UYU"] = new("UYU", "Peso Uruguayo", 2, 858, "$"),
		["UYW"] = new("UYW", "Unidad Previsional", 4, 927, "Db"),
		["UZS"] = new("UZS", "Uzbekistan Sum", 2, 860, "лв"),
		["VES"] = new("VES", "Bolívar Soberano", 2, 928, "Bs."),
		["VND"] = new("VND", "Dong", 0, 704, "₫"),
		["VUV"] = new("VUV", "Vatu", 0, 548, "VT"),
		["WST"] = new("WST", "Tala", 2, 882, "WS$"),
		["XAF"] = new("XAF", "CFA Franc BEAC", 0, 950, "FCFA"),
		["XAG"] = new("XAG", "Silver", 0, 961, Currency.GenericCurrencySign),
		["XAU"] = new("XAU", "Gold", 0, 959, Currency.GenericCurrencySign),
		["XBA"] = new(
			"XBA",
			"Bond Markets Unit European Composite Unit (EURCO)",
			0,
			955,
			Currency.GenericCurrencySign
		),
		["XBB"] = new(
			"XBB",
			"Bond Markets Unit European Monetary Unit (E.M.U.-6)",
			0,
			956,
			Currency.GenericCurrencySign
		),
		["XBC"] = new(
			"XBC",
			"Bond Markets Unit European Unit of Account 9 (E.U.A.-9)",
			0,
			957,
			Currency.GenericCurrencySign
		),
		["XBD"] = new(
			"XBD",
			"Bond Markets Unit European Unit of Account 17 (E.U.A.-17)",
			0,
			958,
			Currency.GenericCurrencySign
		),
		["XCD"] = new("XCD", "East Caribbean Dollar", 2, 951, "$"),
		["XDR"] = new("XDR", "SDR (Special Drawing Right)", 0, 960, Currency.GenericCurrencySign),
		["XOF"] = new("XOF", "CFA Franc BCEAO", 0, 952, "CFA"),
		["XPD"] = new("XPD", "Palladium", 0, 964, Currency.GenericCurrencySign),
		["XPF"] = new("XPF", "CFP Franc", 0, 953, "F"),
		["XPT"] = new("XPT", "Platinum", 0, 962, Currency.GenericCurrencySign),
		["XSU"] = new("XSU", "Sucre", 0, 994, Currency.GenericCurrencySign),
		["XTS"] = new(
			"XTS",
			"Codes specifically reserved for testing purposes",
			0,
			963,
			Currency.GenericCurrencySign
		),
		["XUA"] = new("XUA", "ADB Unit of Account", 0, 965, Currency.GenericCurrencySign),
		["XXX"] = new(
			"XXX",
			"The codes assigned for transactions where no currency is involved",
			0,
			999,
			Currency.GenericCurrencySign
		),
		["YER"] = new("YER", "Yemeni Rial", 2, 886, "﷼"),
		["ZAR"] = new("ZAR", "Rand", 2, 710, "R"),
		["ZMW"] = new("ZMW", "Zambian Kwacha", 2, 967, "ZK"),
		["ZWL"] = new("ZWL", "Zimbabwe Dollar", 2, 932, "$"),
	};

	public Currency GetByCode(string code)
	{
		if (_currencies.TryGetValue(code, out var currency))
		{
			return currency;
		}

		throw new ArgumentOutOfRangeException(
			nameof(code),
			$"Currency '{code}' is not registered in {GetType().FullName}"
		);
	}
}
