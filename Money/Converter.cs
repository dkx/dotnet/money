namespace DKX.NMoney;

public sealed class Converter
{
	private readonly IExchange _exchange;

	public Converter(IExchange exchange)
	{
		_exchange = exchange;
	}

	public async Task<Money> Convert(
		Money fromMoney,
		Currency targetCurrency,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
	{
		var ratio = await _exchange.GetConversionRatio(fromMoney.Currency, targetCurrency);
		var amount = Math.Round(
			fromMoney.ToDecimal(roundingMode) * ratio.Rate,
			targetCurrency.MinorUnit,
			roundingMode
		);

		return new Money(amount, targetCurrency);
	}
}
