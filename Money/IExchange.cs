namespace DKX.NMoney;

public interface IExchange
{
	Task<ExchangeRate> GetConversionRatio(Currency originalCurrency, Currency targetCurrency);
}
