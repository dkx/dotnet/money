﻿namespace DKX.NMoney;

public readonly struct Money : IComparable<Money>
{
	public Money(long amount, Currency currency)
	{
		Amount = amount;
		Currency = currency;
	}

	public Money(long amount, string currency)
		: this(amount, new Currency(currency)) { }

	public Money(
		decimal amount,
		Currency currency,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
		: this(DecimalToLong(amount, currency, roundingMode), currency) { }

	public Money(
		decimal amount,
		string currency,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
		: this(amount, new Currency(currency), roundingMode) { }

	public long Amount { get; }

	public Currency Currency { get; }

	public Money NewInstance(long amount)
	{
		return new Money(amount, Currency);
	}

	public Money NewInstance(
		decimal amount,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
	{
		return new Money(amount, Currency, roundingMode);
	}

	public bool IsSameCurrency(Money other)
	{
		return Currency == other.Currency;
	}

	public void AssertSameCurrency(Money other)
	{
		if (!IsSameCurrency(other))
		{
			throw new ArgumentException("Currencies must be identical");
		}
	}

	public override int GetHashCode()
	{
		return HashCode.Combine(Amount, Currency);
	}

	public bool Equals(Money other)
	{
		return Amount == other.Amount && Currency.Equals(other.Currency);
	}

	public override bool Equals(object? obj)
	{
		return obj is Money other && Equals(other);
	}

	public int CompareTo(Money other)
	{
		AssertSameCurrency(other);
		return Amount.CompareTo(other.Amount);
	}

	public bool GreaterThan(Money other)
	{
		AssertSameCurrency(other);
		return Amount > other.Amount;
	}

	public bool GreaterThanOrEqual(Money other)
	{
		AssertSameCurrency(other);
		return Amount >= other.Amount;
	}

	public bool LessThan(Money other)
	{
		AssertSameCurrency(other);
		return Amount < other.Amount;
	}

	public bool LessThanOrEqual(Money other)
	{
		AssertSameCurrency(other);
		return Amount <= other.Amount;
	}

	public Money Add(Money add)
	{
		AssertSameCurrency(add);
		return NewInstance(Amount + add.Amount);
	}

	public Money Add(decimal add, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return NewInstance(Amount + DecimalToLong(add, Currency, roundingMode));
	}

	public Money Subtract(Money subtract)
	{
		AssertSameCurrency(subtract);
		return NewInstance(Amount - subtract.Amount);
	}

	public Money Subtract(decimal subtract, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return NewInstance(Amount - DecimalToLong(subtract, Currency, roundingMode));
	}

	public Money Multiply(Money multiplier, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		AssertSameCurrency(multiplier);
		return Multiply(multiplier.ToDecimal(roundingMode), roundingMode);
	}

	public Money Multiply(
		decimal multiplier,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
	{
		return NewInstance(Convert.ToInt64(Math.Round(Amount * multiplier, roundingMode)));
	}

	public Money Multiply(
		double multiplier,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
	{
		return Multiply(Convert.ToDecimal(multiplier), roundingMode);
	}

	public Money Multiply(int multiplier, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return Multiply(Convert.ToDecimal(multiplier), roundingMode);
	}

	public Money Divide(Money divisor, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return Divide(divisor.ToDecimal(roundingMode), roundingMode);
	}

	public Money Divide(decimal divisor, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		if (divisor == 0)
		{
			throw new DivideByZeroException();
		}

		return NewInstance(Convert.ToInt64(Math.Round(Amount / divisor, roundingMode)));
	}

	public Money Divide(double divisor, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return Divide(Convert.ToDecimal(divisor), roundingMode);
	}

	public Money Divide(int divisor, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return Divide(Convert.ToDecimal(divisor), roundingMode);
	}

	public Money Mod(decimal divisor, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return NewInstance(Convert.ToInt64(Math.Round(Amount % divisor, roundingMode)));
	}

	public Money Mod(double divisor, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return Mod(Convert.ToDecimal(divisor), roundingMode);
	}

	public Money Mod(int divisor, MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return Mod(Convert.ToDecimal(divisor), roundingMode);
	}

	public Money Abs()
	{
		return NewInstance(Math.Abs(Amount));
	}

	public Money Negative()
	{
		if (Amount <= 0)
		{
			return NewInstance(Amount);
		}

		return NewInstance(0) - this;
	}

	public IReadOnlyCollection<Money> Allocate(IReadOnlyCollection<decimal> ratios)
	{
		var totalCount = ratios.Count;
		if (totalCount < 1)
		{
			throw new ArgumentException("Called allocate on an empty array");
		}

		var total = ratios.Sum();
		if (total <= 0)
		{
			throw new ArgumentException("Cannot allocate: sum of ratios must be greater than zero");
		}

		var remainder = Amount;
		var shares = new Money[totalCount];
		var fractions = new decimal[totalCount];

		for (var i = 0; i < totalCount; i++)
		{
			var ratio = ratios.ElementAt(i);
			if (ratio < 0)
			{
				throw new ArgumentException(
					"Cannot allocate: ratio must be zero or positive number"
				);
			}

			var share = Convert.ToInt64(Math.Floor((Amount * ratio) / total));
			var fractionShare = (ratio / total) * Amount;

			shares[i] = NewInstance(share);
			fractions[i] = fractionShare - Math.Floor(fractionShare);
			remainder -= share;
		}

		if (remainder == 0)
		{
			return shares;
		}

		while (remainder > 0)
		{
			var index = fractions.Length < 1 ? 0 : Array.IndexOf(fractions, fractions.Max());
			shares[index] += NewInstance(1);
			remainder--;
			fractions[index] = -1;
		}

		return shares;
	}

	public IReadOnlyCollection<Money> AllocateTo(uint n)
	{
		if (n < 1)
		{
			throw new ArgumentException("Can not allocate to zero or negative number of shares");
		}

		var ratios = new decimal[n];
		Array.Fill(ratios, 1);

		return Allocate(ratios);
	}

	public decimal RatioOf(Money other)
	{
		AssertSameCurrency(other);

		if (other.IsZero())
		{
			throw new ArgumentException("Cannot calculate a ratio of zero");
		}

		return decimal.Divide(Amount, other.Amount);
	}

	public decimal PercentageDifference(Money other)
	{
		AssertSameCurrency(other);

		var a = ToDecimal();
		var b = other.ToDecimal();

		return 100 * Math.Abs((a - b) / ((a + b) / 2));
	}

	public decimal PercentageChange(Money other)
	{
		AssertSameCurrency(other);

		var a = ToDecimal();
		var b = other.ToDecimal();

		var result = Math.Abs(a - b) / a * 100;

		if (a <= b)
		{
			return result;
		}

		return -result;
	}

	public bool IsZero()
	{
		return Amount == 0;
	}

	public bool IsPositive()
	{
		return Amount > 0;
	}

	public bool IsNegative()
	{
		return Amount < 0;
	}

	public decimal ToDecimal(MidpointRounding roundingMode = MidpointRounding.ToEven)
	{
		return LongToDecimal(Amount, Currency, roundingMode);
	}

	public static Money Min(Money a, Money b)
	{
		a.AssertSameCurrency(b);
		return a <= b ? a : b;
	}

	public static Money Min(IReadOnlyCollection<Money> collection)
	{
		if (collection.Count < 1)
		{
			throw new ArgumentException("Called min on an empty array");
		}

		return collection.Min();
	}

	public static Money Max(Money a, Money b)
	{
		a.AssertSameCurrency(b);
		return a >= b ? a : b;
	}

	public static Money Max(IReadOnlyCollection<Money> collection)
	{
		if (collection.Count < 1)
		{
			throw new ArgumentException("Called max on an empty array");
		}

		return collection.Max();
	}

	public static Money Sum(IReadOnlyCollection<Money> collection)
	{
		if (collection.Count < 1)
		{
			throw new ArgumentException("Called sum on an empty array");
		}

		var currency = collection.First().Currency;
		var result = new Money(0, currency);

		return collection.Aggregate(result, (current, item) => current.Add(item));
	}

	public static Money Average(IReadOnlyCollection<Money> collection)
	{
		var totalCount = collection.Count;
		if (totalCount < 1)
		{
			throw new ArgumentException("Called Average on an empty array");
		}

		return Sum(collection).Divide(totalCount);
	}

	public static Money WeightedAverage(
		IReadOnlyCollection<KeyValuePair<Money, decimal>> collection
	)
	{
		if (collection.Count < 1)
		{
			throw new ArgumentException("Called WeightedAverage on an empty collection");
		}

		var (first, _) = collection.FirstOrDefault();

		if (collection.Count == 1)
		{
			return first;
		}

		var result = first.NewInstance(0);

		foreach (var (price, weight) in collection)
		{
			first.AssertSameCurrency(price);
			result = result.Add(price.Multiply(weight));
		}

		return result;
	}

	public static bool operator ==(Money? a, Money? b)
	{
		if (a is null && b is null)
		{
			return true;
		}

		if (a is null || b is null)
		{
			return false;
		}

		return a.Equals(b);
	}

	public static bool operator !=(Money? a, Money? b)
	{
		if (a is null && b is null)
		{
			return false;
		}

		if (a is null || b is null)
		{
			return true;
		}

		return !a.Equals(b);
	}

	public static bool operator >(Money a, Money b)
	{
		return a.GreaterThan(b);
	}

	public static bool operator >=(Money a, Money b)
	{
		return a.GreaterThanOrEqual(b);
	}

	public static bool operator <(Money a, Money b)
	{
		return a.LessThan(b);
	}

	public static bool operator <=(Money a, Money b)
	{
		return a.LessThanOrEqual(b);
	}

	public static Money operator +(Money a, Money b)
	{
		return a.Add(b);
	}

	public static Money operator +(decimal a, Money b)
	{
		return b.Add(a);
	}

	public static Money operator +(Money a, decimal b)
	{
		return a.Add(b);
	}

	public static Money operator -(Money a, Money b)
	{
		return a.Subtract(b);
	}

	public static Money operator -(decimal a, Money b)
	{
		return new Money(a - b.ToDecimal(), b.Currency);
	}

	public static Money operator -(Money a, decimal b)
	{
		return a.Subtract(b);
	}

	public static Money operator *(Money a, Money b)
	{
		return a.Multiply(b);
	}

	public static Money operator *(Money a, decimal b)
	{
		return a.Multiply(b);
	}

	public static Money operator *(decimal a, Money b)
	{
		return b.Multiply(a);
	}

	public static Money operator *(Money a, double b)
	{
		return a.Multiply(b);
	}

	public static Money operator *(double a, Money b)
	{
		return b.Multiply(a);
	}

	public static Money operator *(Money a, int b)
	{
		return a.Multiply(b);
	}

	public static Money operator *(int a, Money b)
	{
		return b.Multiply(a);
	}

	public static Money operator /(Money a, Money b)
	{
		return a.Divide(b);
	}

	public static Money operator /(Money a, decimal b)
	{
		return a.Divide(b);
	}

	public static Money operator /(Money a, double b)
	{
		return a.Divide(b);
	}

	public static Money operator /(Money a, int b)
	{
		return a.Divide(b);
	}

	public static Money operator %(Money a, decimal b)
	{
		return a.Mod(b);
	}

	public static Money operator %(Money a, double b)
	{
		return a.Mod(b);
	}

	public static Money operator %(Money a, int b)
	{
		return a.Mod(b);
	}

	public static explicit operator decimal(Money m)
	{
		return m.ToDecimal();
	}

	private static long DecimalToLong(
		decimal amount,
		Currency currency,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
	{
		amount = Math.Round(amount, currency.MinorUnit, roundingMode);
		return Convert.ToInt64(amount * Convert.ToDecimal(Math.Pow(10, currency.MinorUnit)));
	}

	private static decimal LongToDecimal(
		long amount,
		Currency currency,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
	{
		if (currency.MinorUnit < 1)
		{
			return amount;
		}

		return Math.Round(
			decimal.Divide(amount, Convert.ToInt64(Math.Pow(10, currency.MinorUnit))),
			currency.MinorUnit,
			roundingMode
		);
	}
}
