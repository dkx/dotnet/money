using System.Globalization;
using System.Text.RegularExpressions;

namespace DKX.NMoney.Parser;

public sealed class DecimalParser : IParser
{
	private const string Pattern = @"^(?<sign>-)?(?<digits>0|[1-9]\d*)?\.?(?<fraction>\d+)?$";

	public Money Parse(string money)
	{
		throw new NotImplementedException();
	}

	public Money Parse(
		string money,
		Currency currency,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
	{
		return Parse(money, currency, currency.MinorUnit, roundingMode);
	}

	public Money Parse(
		string money,
		Currency currency,
		byte minorUnits,
		MidpointRounding roundingMode = MidpointRounding.ToEven
	)
	{
		if (string.IsNullOrWhiteSpace(money))
		{
			return new Money(0, currency);
		}

		var match = Regex.Match(money, Pattern);
		if (
			!match.Success || (!match.Groups["digits"].Success && !match.Groups["fraction"].Success)
		)
		{
			throw new ArgumentException(
				$"Can not parse \"{money}\" with currency \"{currency.Code}\""
			);
		}

		var value = match.Groups["digits"].Success
			? decimal.Parse(match.Groups["digits"].Value, CultureInfo.InvariantCulture)
			: 0m;

		if (match.Groups["fraction"].Success)
		{
			value += decimal.Parse(
				"0." + match.Groups["fraction"].Value,
				CultureInfo.InvariantCulture
			);
		}

		value = Math.Round(value, minorUnits, roundingMode);
		value *= Convert.ToInt64(Math.Pow(10, minorUnits));

		var result = new Money(Convert.ToInt64(value), currency);
		if (match.Groups["sign"].Success && match.Groups["sign"].Value == "-")
		{
			result = result.Negative();
		}

		return result;
	}
}
