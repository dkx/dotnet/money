namespace DKX.NMoney;

public interface IParser
{
	Money Parse(string money);
}
