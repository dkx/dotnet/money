namespace DKX.NMoney;

public interface IFormatter
{
	string Format(Money money);
}
