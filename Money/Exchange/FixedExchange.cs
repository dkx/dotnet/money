namespace DKX.NMoney.Exchange;

public sealed class FixedExchange : IExchange
{
	private readonly IReadOnlyCollection<ExchangeRate> _list;

	public FixedExchange(IReadOnlyCollection<ExchangeRate> list)
	{
		_list = list;
	}

	public Task<ExchangeRate> GetConversionRatio(Currency originalCurrency, Currency targetCurrency)
	{
		foreach (var rate in _list)
		{
			if (rate.Source == originalCurrency && rate.Target == targetCurrency)
			{
				return Task.FromResult(rate);
			}
		}

		throw new ArgumentException(
			$"Exchange rate for {originalCurrency.Code} > {targetCurrency.Code} is not set"
		);
	}
}
