using System.Text.Json.Serialization;

namespace DKX.NMoney.Exchange.Fixer;

public sealed record FixerResponseError
{
	[JsonPropertyName("code")]
	public ushort Code { get; init; }

	[JsonPropertyName("type")]
	public string Type { get; init; } = "error";
}
