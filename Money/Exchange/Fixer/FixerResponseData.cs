using System.Text.Json.Serialization;

namespace DKX.NMoney.Exchange.Fixer;

public sealed record FixerResponseData
{
	[JsonPropertyName("success")]
	public bool Success { get; init; } = true;

	[JsonPropertyName("error")]
	public FixerResponseError? Error { get; init; }

	[JsonPropertyName("rates")]
	public IDictionary<string, decimal>? Rates { get; init; }
}
