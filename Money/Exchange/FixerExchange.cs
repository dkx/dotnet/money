using System.Text.Json;
using DKX.NMoney.Exchange.Fixer;

namespace DKX.NMoney.Exchange;

public sealed class FixerExchange : IExchange
{
	public const string HttpClientName = "FixerExchange";

	private readonly IHttpClientFactory _httpClientFactory;

	private readonly string _apiKey;

	private readonly bool _https;

	public FixerExchange(IHttpClientFactory httpClientFactory, string apiKey, bool https = true)
	{
		_httpClientFactory = httpClientFactory;
		_apiKey = apiKey;
		_https = https;
	}

	public async Task<ExchangeRate> GetConversionRatio(
		Currency originalCurrency,
		Currency targetCurrency
	)
	{
		using var http = _httpClientFactory.CreateClient(HttpClientName);

		var uri = CreateUri(originalCurrency, targetCurrency);
		var res = await http.GetStringAsync(uri);
		var data = JsonSerializer.Deserialize<FixerResponseData>(res);

		if (data is null)
		{
			throw new ArgumentException($"FixerExchange: {uri} returned null");
		}

		if (data.Error is { } error)
		{
			throw new ArgumentException(error.Type);
		}

		if (data.Rates is null)
		{
			throw new ArgumentException($"FixedExchange: {uri} did not return any rates");
		}

		foreach (var (key, value) in data.Rates)
		{
			if (string.Equals(key, targetCurrency.Code, StringComparison.OrdinalIgnoreCase))
			{
				return new ExchangeRate(originalCurrency, targetCurrency, value);
			}
		}

		throw new ArgumentException(
			$"FixerExchange: could not load rate for {originalCurrency.Code} > {targetCurrency.Code} from {uri}"
		);
	}

	private string CreateUri(Currency originalCurrency, Currency targetCurrency)
	{
		var uriBuilder = new UriBuilder
		{
			Scheme = _https ? "https" : "http",
			Host = "data.fixer.io",
			Path = "/api/latest",
			Query =
				$"access_key={_apiKey}&base={originalCurrency.Code}&symbols={targetCurrency.Code}",
		};

		return uriBuilder.ToString();
	}
}
