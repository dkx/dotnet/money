using System.Text.Json;
using System.Text.Json.Serialization;

namespace DKX.NMoney.Json;

public sealed class MoneyConverter : JsonConverter<Money>
{
	private readonly ICurrencyList _currencies;

	public MoneyConverter(ICurrencyList currencies)
	{
		_currencies = currencies;
	}

	public override Money Read(
		ref Utf8JsonReader reader,
		Type typeToConvert,
		JsonSerializerOptions options
	)
	{
		if (reader.TokenType != JsonTokenType.StartObject)
		{
			throw new JsonException();
		}

		string? currencyCode = null;
		string? amount = null;

		while (reader.Read())
		{
			if (reader.TokenType == JsonTokenType.EndObject)
			{
				break;
			}

			if (reader.TokenType != JsonTokenType.PropertyName)
			{
				throw new JsonException();
			}

			var name = reader.GetString();

			if (!reader.Read())
			{
				throw new JsonException();
			}

			switch (name)
			{
				case "currency":
					currencyCode = reader.GetString();
					break;
				case "amount":
					amount = reader.GetString();
					break;
				default:
					throw new JsonException();
			}
		}

		if (currencyCode is null || amount is null)
		{
			throw new JsonException();
		}

		return new Money(Convert.ToInt64(amount), new Currency(currencyCode, _currencies));
	}

	public override void Write(Utf8JsonWriter writer, Money value, JsonSerializerOptions options)
	{
		writer.WriteStartObject();
		writer.WriteString("currency", value.Currency.Code);
		writer.WriteString("amount", value.Amount.ToString());
		writer.WriteEndObject();
	}
}
