﻿namespace DKX.NMoney;

public interface ICurrencyList
{
	Currency GetByCode(string code);
}
