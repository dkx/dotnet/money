using DKX.NMoney.CurrencyList;

namespace DKX.NMoney;

public readonly struct Currency
{
	public const string GenericCurrencySign = "¤";

	private static readonly ICurrencyList DefaultList = IsoCurrencyList.Instance;

	public Currency(string code, string name, byte minorUnit, ushort numericCode, string symbol)
	{
		Code = code;
		Name = name;
		MinorUnit = minorUnit;
		NumericCode = numericCode;
		Symbol = symbol;
	}

	public Currency(string code, ICurrencyList list)
	{
		var c = list.GetByCode(code);
		Code = c.Code;
		Name = c.Name;
		MinorUnit = c.MinorUnit;
		NumericCode = c.NumericCode;
		Symbol = c.Symbol;
	}

	public Currency(string code)
		: this(code, DefaultList) { }

	public string Code { get; }

	public string Name { get; }

	public byte MinorUnit { get; }

	public ushort NumericCode { get; }

	public string Symbol { get; }

	public override int GetHashCode()
	{
		return HashCode.Combine(Code, NumericCode);
	}

	public bool Equals(Currency other)
	{
		return Code == other.Code && NumericCode == other.NumericCode;
	}

	public override bool Equals(object? obj)
	{
		return obj is Currency other && Equals(other);
	}

	public static bool operator ==(Currency a, Currency b)
	{
		return a.Equals(b);
	}

	public static bool operator !=(Currency a, Currency b)
	{
		return !a.Equals(b);
	}
}
