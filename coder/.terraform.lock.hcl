# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/coder/coder" {
  version     = "0.11.1"
  constraints = "~> 0.11.1"
  hashes = [
    "h1:tGE1dBDTvo6fLjCcNA5smGbjzdzlOmuWwCy1LTmyK3Q=",
    "zh:0dacbf879fbfd988d64a05b639992c8c6be577e451ba6102cc8719e6c43010ca",
    "zh:553b078a2d5a6704753b6acbe82923509d2fbd81c7c197e441c7dd637ea008a0",
    "zh:5830bd13d5de0ed62fdded1e15871e636c4cefca2d2775b05bbbd1991438579e",
    "zh:60b4e1d371b8bf85ea12970cb66ff44c19d4f1bdfbaa60df0b31497fbad3c311",
    "zh:65216cec7a77921ca00269eb3da6e8bc97741099b4f1158045a1972a0b5ff52f",
    "zh:6c3ac480f606607cc0daeede49673bbf1d17fa3a46c90d1ddbb40a5dd25e8d92",
    "zh:7abb5070f2f0d714762dc0fd4bbfcaf7fa08d2d4460f3923a8ddfb6c5c9b0203",
    "zh:8411d7d1bb2db08cbbd1f93240108eb7ea889311527ab21947e263140213f0d6",
    "zh:883590f351eda4d9f2bdf42d784b0243ab57049e78b6e13756e835a19a425510",
    "zh:955cde64638caa728edaac2883a09760005dff921288ffcb876ddfa8376a4119",
    "zh:9d34709908b941531b3eb2832083fc96433d4795bc1b8b6ab79ad6f1a1f33160",
    "zh:a84cfee8100ec6da13ba3a71b43442e36edf2a47822222f68d18c226046c908e",
    "zh:ecdc70d58846a2a877b6d4608e0d260038bae6d866f65c10b814436fd57855e7",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:f8de72a299021bcadd2ed0078729e04a7391ebb6adaa565f4f540facc5ed9586",
  ]
}

provider "registry.terraform.io/kreuzwerker/docker" {
  version     = "3.0.2"
  constraints = "~> 3.0.2"
  hashes = [
    "h1:DcRxJArfX6EiATluWeCBW7HoD6usz9fMoTK2U3dmyPk=",
    "zh:15b0a2b2b563d8d40f62f83057d91acb02cd0096f207488d8b4298a59203d64f",
    "zh:23d919de139f7cd5ebfd2ff1b94e6d9913f0977fcfc2ca02e1573be53e269f95",
    "zh:38081b3fe317c7e9555b2aaad325ad3fa516a886d2dfa8605ae6a809c1072138",
    "zh:4a9c5065b178082f79ad8160243369c185214d874ff5048556d48d3edd03c4da",
    "zh:5438ef6afe057945f28bce43d76c4401254073de01a774760169ac1058830ac2",
    "zh:60b7fadc287166e5c9873dfe53a7976d98244979e0ab66428ea0dea1ebf33e06",
    "zh:61c5ec1cb94e4c4a4fb1e4a24576d5f39a955f09afb17dab982de62b70a9bdd1",
    "zh:a38fe9016ace5f911ab00c88e64b156ebbbbfb72a51a44da3c13d442cd214710",
    "zh:c2c4d2b1fd9ebb291c57f524b3bf9d0994ff3e815c0cd9c9bcb87166dc687005",
    "zh:d567bb8ce483ab2cf0602e07eae57027a1a53994aba470fa76095912a505533d",
    "zh:e83bf05ab6a19dd8c43547ce9a8a511f8c331a124d11ac64687c764ab9d5a792",
    "zh:e90c934b5cd65516fbcc454c89a150bfa726e7cf1fe749790c7480bbeb19d387",
    "zh:f05f167d2eaf913045d8e7b88c13757e3cf595dd5cd333057fdafc7c4b7fed62",
    "zh:fcc9c1cea5ce85e8bcb593862e699a881bd36dffd29e2e367f82d15368659c3d",
  ]
}
